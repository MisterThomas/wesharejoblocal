
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ListOfferFavorite from "./components/Offer/ListOfferFavorite";
import ListOffer from "./components/Offer/ListOffer";
import AddOffer from "./components/Offer/AddOffer";
import OfferId from "./components/Offer/OfferId";
import Login from "./components/User/Login";
import Signup from "./components/User/Signup";
import Account from "./components/User/Account";
import Home from "./components/Home";
import Navbar from "./components/HeaderNavbar";
import Footer from "./components/Footer";
import Logout from "./components/User/Logout";
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import NewList from "./components/Offer/NewList";
import React, {Component} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import UserService from "./services/UserService";

class App extends Component {
  render() {
    return (
        <Router>
          <div>
            <Navbar/>
            <Switch>
                <Route exact path="/" component={Home}/>
            {/*  <Route exact path="/listOfferFavorite" component={ListOfferFavorite}/>*/}
              <AuthenticatedRoute exact path="/listOfferFavorite/:name" component={ListOfferFavorite}/>
              <AuthenticatedRoute exact path="/logout" component={Logout} />
              <Route exact path="/listOffer" component={ListOffer}/>
              <Route exact path="/newList" component={NewList}/>
              <AuthenticatedRoute exact path="/add-offer/:id" component={AddOffer}/>
              <AuthenticatedRoute exact path="/view-offer/:id" component={OfferId}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/signup" component={Signup}/>
            <AuthenticatedRoute exact path="/account" component={Account}/>
            <Route exact path="" component={ErrorComponent} />
            </Switch>
          </div>
          <Footer/>
        </Router>
    );
  }
}

function ErrorComponent() {
    return <div>An error occurred. I don't know what to do. Please contact the support service error@wescrapjob.com</div>
}
export default App;
