import axios from 'axios';

const  urlOffers = "http://localhost:8085/api/offers";
class OfferService {

    getAllOffers(){
        return axios.get(urlOffers + "/listOffers");
    }

    getOffer(id){
        return axios.get(urlOffers + "/listOffers/id/" + id);
    }

    getOfferUsername(username){
        return axios.get(urlOffers + "/listOffers/username/" + username);
    }

    listCompanyOrPostOrCity(company, post, city){
        return axios.post(urlOffers + "/listOffers/search", company, post, city);
    }

    saveOffer(offer){
        return axios.post(urlOffers + "/listOffers/post", offer);
    }

    updateOffer(offer, id){
        return axios.put(urlOffers + `/listOffers/update/${id}`, offer);
    }

    deleteOffer(id){
        return axios.delete(urlOffers + "/listOffers/delete/" + id);
    }
}
export default new OfferService();