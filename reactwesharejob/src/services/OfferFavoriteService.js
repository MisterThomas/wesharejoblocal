import axios from 'axios';

const  urlOfferFavorite = "http://localhost:8085/api/offerFavorites";

class OfferFavoriteService{

    AllOffersFavorites(){
        return axios.get(urlOfferFavorite + "/listOffersFavorites")
    }

    offerFavoritePost(id, offerFavorite){
        return axios.post(urlOfferFavorite + `/listOffers/${id}/add-offerFavorite`, offerFavorite);
    }


    getOfferFavoritesId(id){
        return axios.get(urlOfferFavorite + "/listOffersFavorites/" + id);
    }

    getOfferFavoriteUsername(username){

      //  console.log(urlOfferFavorite + "/listOffersFavorites/username/" + username);
        return axios.get(urlOfferFavorite + "/listOffersFavorites/username/" + username);
    }

    deleteOfferFavorite(id){
        return axios.delete(urlOfferFavorite + "/listOffersFavorites/delete/"+ id);
    }
}
export default new OfferFavoriteService();