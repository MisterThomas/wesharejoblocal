import {Component} from "react";
import UserService from "../services/UserService";
import { Route, Redirect } from 'react-router-dom'


class AuthenticatedRoute extends Component {

    render() {
        if (UserService.isUserLoggedIn()) {
            return <Route {...this.props} />
        } else {
            return <Redirect to="/login" />
        }

    }

}
export default AuthenticatedRoute