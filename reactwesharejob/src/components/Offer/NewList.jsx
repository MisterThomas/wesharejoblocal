import React, { Component } from 'react'
import OfferFavoriteService from "../../services/OfferFavoriteService";
import OfferService from "../../services/OfferService";
import "./css/ListOffer.css"
import axios from 'axios';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faList, faEdit, faTrash, faSearch, faTimes, faSave, faEnvelopeOpenText, faLink} from '@fortawesome/free-solid-svg-icons';
import {Card, Button, InputGroup, FormControl} from 'react-bootstrap';
import MyToast from "./MyToast";

class NewList extends Component {

    constructor(props) {

        super(props)

        this.state = {
            listOffersFavorite: [],
            username: props.match.params.name,
            search: '',
        }
        this.addOffer = this.addOffer.bind(this);
        this.updateOffer = this.updateOffer.bind(this);
        this.deleteOffer = this.deleteOffer.bind(this);
        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    /*
        sortData = () => {
            setTimeout(() => {
                this.state.sortDir === "asc" ? this.setState({sortDir: "desc"}) : this.setState({sortDir: "asc"});
                this.props.listOffers(this.state.currentPage);
            }, 500);
        };
    */

    componentDidMount() {
        OfferFavoriteService.getAllOffersFavorites().then((res) => {
            console.log(res.data)
            this.setState({listOffersFavorite: res.data});
        });

        const rememberMe = localStorage.getItem('rememberMe') === 'true';
        const user = rememberMe ? localStorage.getItem('user') : '';
        this.setState({ user, rememberMe });
    }

    handleChange(event) {
        this.setState({
                [event.target.name]
                    :event.target.value
            }
        )
    }

    addOffer(){
        this.props.history.push('/add-offer/_add');
    }

    offerId(id){
        this.props.history.push(`/view-offer/${id}`);
    }

    updateOffer(id){
        this.props.history.push(`/add-offer/${id}`);
    }

    deleteOffer(id){
        OfferService.deleteOffer(id).then( res => {
            this.setState({listOffers: this.state.listOffers.filter(offer => offer.id !== id)});
        });
    }

    search(){
        axios.get(`http://localhost:8085/api/offers/listOffers/search`, {
            params: {
                city: this.state.search,
                offer: this.state.search,
                post: this.state.search
            }
        })
            .then(response => response.data)
            .then((data) => {
                this.setState({listOffers: data});
            });
    }


    cancelSearch = () => {
        this.setState({"search" : ''});
        OfferService.getAllOffers().then((res) => {
            this.setState({ listOffers: res.data});

        });
    };


    render() {

        console.log(this.state.listOffers);
        const {offers, currentPage, totalPages, search} = this.state;
        return (
            <div>
                <h2 className="text-center"> <FontAwesomeIcon icon={faList} />List Offer</h2>
                <div style={{"display":this.state.show ? "block" : "none"}}>
                    <MyToast show = {this.state.show} message = {"Offer Deleted Successfully."} type = {"danger"}/>
                </div>
                <Card>
                    <Card.Header>
                        <div style={{"float":"right"}}>
                            <InputGroup size="sm">
                                <FormControl placeholder="Search" name="search"
                                             className={"info-border bg-dark text-white"} value={this.state.search}  onChange={this.handleChange}/>
                                <InputGroup.Append>
                                    <Button id="addButton" size="sm" variant="outline-info" type="button"  onClick={this.search}>
                                        <FontAwesomeIcon icon={faSearch}/>
                                    </Button>
                                    <Button size="sm" variant="outline-danger" type="button" onClick={this.cancelSearch}>
                                        <FontAwesomeIcon icon={faTimes} />
                                    </Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </Card.Header>
                    {/*                   <Card.Body>*/}
                    <div  className="row">
                        <button  size="sm" className="btn btn-primary" onClick={this.addOffer}><FontAwesomeIcon icon={faSave}/> Add Offer</button>
                    </div>
                    <br></br>
                    <div className="row">
                        <table className="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th> Company</th>
                                <th> City</th>
                                <th> post</th>
                                <th> url website</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.listOffersFavorite.map(
                                    offer =>
                                        <tr key={offer.id}>
                                            <td> {offer.company}</td>
                                            <td> {offer.city}</td>
                                            <td> {offer.post}</td>
                                            <td><button  className="btn btn-info"><FontAwesomeIcon icon={faLink} /><a href={offer.url}>url</a></button></td>
                                            <td>
                                                <button onClick={() => this.updateOffer(offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faEdit} />Update Offer
                                                </button>
                                                <button style={{marginLeft: "10px"}}
                                                        onClick={() => this.deleteOffer(offer.id)}
                                                        className="btn btn-danger"><FontAwesomeIcon icon={faTrash} />Delete
                                                </button>
                                                <button style={{marginLeft: "10px"}}
                                                        onClick={() => this.offerId(offer.id)}
                                                        className="btn btn-info"><FontAwesomeIcon icon={faEnvelopeOpenText} />View
                                                </button>
                                                {/*<button style={{marginLeft: "10px"}} onClick={ () => this.addOfferFavorite(offer.id)} className="btn btn-info">Add OfferFavorite</button>*/}
                                            </td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                </Card>
            </div>
        )
    }
}
export default NewList;