import OfferService from "../../services/OfferService";
import {Component} from "react";


class OfferId extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            offer: {}
        }
    }

    componentDidMount(){
        OfferService.getOffer(this.state.id).then( res => {
            this.setState({offer: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View Offer Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> company: </label>
                            <div> { this.state.offer.company }</div>
                        </div>
                        <div className = "row">
                            <label> post: </label>
                            <div> { this.state.offer.city }</div>
                        </div>
                        <div className = "row">
                            <label> post: </label>
                            <div> { this.state.offer.post }</div>
                        </div>
                        <div className = "row">
                            <label> url: </label>
                            <div> { this.state.offer.url }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }

} export default OfferId;