import {Component} from "react";


class Footer extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        return (
            <div>
                <footer className = "footer">
                    <p className="footer-copyright mb-0">
                        &copy; {new Date().getFullYear()} Copyright The website is code by Tharsos
                    </p>
                </footer>
            </div>
        )
    }
}

export default Footer