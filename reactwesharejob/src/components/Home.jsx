import React, {Component} from 'react'
import { Link } from 'react-router-dom';
import { Jumbotron, Row, Col, Image, Button, Container } from 'react-bootstrap';
import  './Home.css'


export default class Home extends Component {

render() {
    return (
        <Container  fluid>
            <Container fluid>
                <Jumbotron className="containerwescrapjob">
                    <h1 className="titre">WeShareJob</h1>
                    <p className="description">
                        This is marketplace we scrap job tech offer.we scrap and share 2000 new offer by day.You can find
                        offer of pole emploi, welcome to jungle, indeed,Linkedin.
                    </p>
                    <Link to="/listoffer">
                        <Button className="btn btn-primary" id="listoffer">Find Offer</Button>
                    </Link>
                </Jumbotron>
            </Container>
                <Container>
                    <div className="container-fluid">
                        <div className="text-center">
                            <h2>Are you interested to work with team ?</h2>
                        </div>
                        <div className="row">
                            <div className="col-sm-4">
                                <div className="panel panel-default text-center">
                                    <div className="panel-heading">
                                        <h1>Basic</h1>
                                    </div>
                                    <div className="panel-body">
                                        <p>Your offer is published in website</p>
                                        <p> The publication is promote one day</p>
                                        <p>You have to completed the doc</p>
                                    </div>
                                    <div className="panel-footer">
                                        <h3>5€</h3>
                                        <h4>One Day</h4>
                                        <Button className="btn btn-primary" id="typeform" href="https://form.typeform.com/to/jxEGKkTi">Contact</Button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="panel panel-default text-center">
                                    <div className="panel-heading">
                                        <h1>Pro</h1>
                                    </div>
                                    <div className="panel-body">
                                        <p>All your offers is published in website</p>
                                        <p> The publication is promote one month</p>
                                        <p>You have to completed the doc</p>
                                    </div>
                                    <div className="panel-footer">
                                        <h3>$30</h3>
                                        <h4>per month</h4>
                                        <Button className="btn btn-primary" id="typeform" href="https://form.typeform.com/to/jxEGKkTi">Contact</Button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="panel panel-default text-center">
                                    <div className="panel-heading">
                                        <h1>Premium</h1>
                                    </div>
                                    <div className="panel-body">
                                        <p>All your offers is published in website</p>
                                        <p> The publication is promote one month</p>
                                        <p>You have to completed the doc</p>
                                    </div>
                                    <div className="panel-footer">
                                        <h3>$200</h3>
                                        <h4>per year</h4>
                                        <Button className="btn btn-primary" id="typeform" href="https://form.typeform.com/to/jxEGKkTi">Contact</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Container>

            <Container className="containercontact" fluid>
                <Col xs={12} sm={12}>
                    <h2>Are you interested for  partnership ?</h2>
                    <p> You can completed the docs  and explain your value. </p>
                    <p> We contact you a soon. </p>
                    <p> You have to click and fill up the typeform</p>
                    <Button className="btn btn-primary" id="typeform" href="https://form.typeform.com/to/jxEGKkTi">Contact</Button>
                </Col>
            </Container>
        </Container>
    )
}}

   /*         <div>
                <container>
                    <Col xs={12} sm={12} smOffset={2}>
                        <h2>Are you intersted by partenship</h2>
                        <p>If you have intrested offer to share with us. </p>
                        <p>you can sent email at:</p>
                        <h3 className={this.email}>sales@wescrapjob.com.</h3>
                    </Col>
                </container>
            </div>*/

/*            <div>
                <container>
                    <Col xs={12} sm={8} smOffset={2}>
                        <Image src="assets/person-1.jpg" className="about-profile-pic" rounded />
                        <h3>You have question of business ?</h3>
                        <p>The business respect RGPD. If you have problem with the business of wescrapJob.</p>
                        <p>You can adresse mail at : problem@wescrapJob.com and the lawyer of company M.Mathieu DEMOS mathieu.demos@lawmin.com</p>
                    </Col>
                </container>
            </div>*/
