import React, { Component } from 'react';
import {Row, Col, Card, Form, InputGroup, FormControl, Button} from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faSignInAlt, faEnvelope, faLock, faUndo} from "@fortawesome/free-solid-svg-icons";
import "./Login.css"
import UserService from "../../services/UserService";
import {User} from "../../model/User";

export default class Login extends Component {




    constructor(props) {
        super(props);

        if (UserService.getLoggedInUserName()) {
            this.props.history.push('/account');
        }


        this.state = {
            user: new User('',''),
            hasLoginFailed: false,
            loading: false,
            showSuccessMessage: false
        }
        this.handleChange =this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);

    }



    handleChange(e){
        console.log(this.state.user)
        var { name, value } = e.target;
        var user = this.state.user;
        user[name] = value;
        this.setState({ user: user });
    }



    handleLogin(e) {
        console.log(e);
        e.preventDefault();

        this.setState({ showSuccessMessage: true });
        const { user } = this.state;

        // stop here if form is invalid
        if (!(user.username && user.password)) {
            return;
        }

        this.setState({ loading: true });
        UserService.login(user)
            .then(
                data => {
                    this.props.history.push(`/ListOfferFavorite/${user.username}`);
                },
                error => {
                    console.log(error);
                    this.setState({showSuccessMessage:false, hasLoginFailed:true, loading: false });
                }
            );
    }



    credentialChange = event => {
        this.setState({
            [event.target.name] : event.target.value
        });
    };


    render() {

       const {user,showSuccessMessage,  loading} = this.state;

        return (
            <div className="col-md-12">
                <div className="card card-container">
                    <img id="profile-img" className="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                    <ShowInvalidCredentials hasLoginFailed={this.state.hasLoginFailed}/>
                    <ShowLoginSucessMessage showSuccessMessage={this.state.showSuccessMessage} />
                    <form name="form" onSubmit={(e) => this.handleLogin(e)}>
                        <div className={'form-group' + (showSuccessMessage && !user.username ? ' has-error' : '')}>
                            <label htmlFor="username">Username</label>
                            <input type="text" className="form-control" name="username" value={user.username} onChange={(e) => this.handleChange(e)} />
                            {showSuccessMessage && !user.username &&
                            <div className="help-block">Username is required</div>
                            }
                        </div>
                        <div className={'form-group' + (showSuccessMessage && !user.password ? ' has-error' : '')}>
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" name="password" value={user.password} onChange={(e) => this.handleChange(e)} />
                            {showSuccessMessage && !user.password &&
                            <div className="help-block">Password is required</div>
                            }
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary btn-block" disabled={loading}>Login</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function ShowInvalidCredentials(props) {
            if(props.hasLoginFailed) {
                return <div className="alert alert-warning">Invalid Credentials</div>
            } else {
                return null
            }
}


function ShowLoginSucessMessage(props) {
    if(props.showSuccessMessage) {
        return <div className="">Login Successful</div>
    } else {
        return null
    }
}
