import React, { Component } from 'react';
import {Row, Col, Card, Form, InputGroup, FormControl, Button} from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faEnvelope, faLock, faUndo, faUserPlus, faUser} from "@fortawesome/free-solid-svg-icons";
import './Signup.css'
import UserService from "../../services/UserService";
import {User} from "../../model/User";

export default class Signup extends Component {
    constructor(props) {
        super(props);


        if (UserService.getLoggedInUserName()) {
            this.props.history.push('/account');
        }


        this.state = {
            user: new User('','',''),
            id: this.props.id,
            active: false,
            roles: 'USER',
            hasLoginFailed: false,
            showSuccessMessage: false,
            loading: false,
            submitted: false
        }

        this.handleChange =this.handleChange.bind(this);
        this.handleRegister=this.handleRegister.bind(this);
    }


    handleRegister(e) {
        e.preventDefault();
        this.setState({submitted: true,});
        const{user} = this.state;

        if(!(user.username && user.password && user.email)){
            return;
        }

        this.setState({loading: true});
        user.id = this.state.id;
        user.active = this.state.active;
        user.roles = this.state.roles;
        console.log(user);
        UserService.register(user)
            .then(
                data => {
                    this.props.history.push("/login");
                },
                error => {
                    if(error.response.status === 409){
                        this.setState({
                            errorMessage: "Username is not available",
                            loading: false
                        });
                    }else{
                        this.setState({
                            errorMessage: "Unexpected error occurred.",
                            loading: false
                        });
                    }
                }
            );
    }



    handleChange(e) {
        var {name, value} = e.target;
        var user = this.state.user;
        user[name] = value;
        this.setState({user: user});
    }


    credentialChange = event => {
        this.setState({
            [event.target.name] : event.target.value
        });
    };

    render() {

            const {user,hasLoginFailed,  loading, submitted} = this.state;
        return (
        <div className="col-md-12">
            <div className="card card-container">
                <img id="profile-id" className="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>
                <form name="form" onSubmit={(e) => this.handleRegister(e)}>
                    <div className={'form-group' + (hasLoginFailed && user.email ? 'has-error': '')}>
                        <label htmlFor="email">Full Email</label>
                        <input type="text" className="form-control" name="email" value={user.email} onChange={(e)=>this.handleChange(e)}/>
                        {hasLoginFailed && !user.email &&
                        <div className="alert alert-danger" role="alert">Full email is required.</div>
                        }
                    </div>

                    <div className={'form-group' + (hasLoginFailed && user.username ? 'has-error': '')}>
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" name="username" value={user.username} onChange={(e)=>this.handleChange(e)}/>
                        {hasLoginFailed && !user.username &&
                        <div className="alert alert-danger" role="alert">Username is required.</div>
                        }
                    </div>

                    <div className={'form-group' + (hasLoginFailed && user.password ? 'has-error': '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" name="password" value={user.password} onChange={(e)=>this.handleChange(e)}/>
                        {hasLoginFailed && !user.password &&
                        <div className="alert alert-danger" role="alert">Password is required.</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary btn-block" disabled={loading}>Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
        );
    }
}

function ShowInvalidCredentials(props) {
    if(props.hasLoginFailed) {
        return <div className="alert alert-warning">Invalid Credentials</div>
    } else {
        return null
    }
}


function ShowLoginSucessMessage(props) {
    if(props.showSuccessMessage) {
        return <div className="">Login Successful</div>
    } else {
        return null
    }
}