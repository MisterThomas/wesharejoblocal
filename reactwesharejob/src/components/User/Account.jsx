import React, { Component } from 'react';
import {Row} from 'react-bootstrap';
import "./Account.css"
import {Link} from "react-router-dom";

export default class Account extends Component {

    constructor(props) {
        super(props)
        this.state = {
            welcomeMessage: ''
        }
        this.handleSuccessfulResponse = this.handleSuccessfulResponse.bind(this)
        this.handleError = this.handleError.bind(this)
    }

    render() {
    //    const {email, username} = this.state;

        return (
            <Row className="justify-content-md-center">
                    <h1>Welcome!</h1>
                    <div className="container">
                        Welcome {this.props.match.params.name}.
                        You can check offers <Link to="/listoffer">here</Link>.
                    </div>
                    <div className="container">
                        {this.state.welcomeMessage}
                    </div>
            </Row>
        );
    }

    handleSuccessfulResponse(response) {
        console.log(response)
        this.setState({ welcomeMessage: response.data.message })
    }

    handleError(error) {

        console.log(error.response)

        let errorMessage = '';

        if (error.message)
            errorMessage += error.message

        if (error.response && error.response.data) {
            errorMessage += error.response.data.message
        }

        this.setState({ welcomeMessage: errorMessage })
    }
}