import React, {Component} from "react";
import {Navbar, Nav} from 'react-bootstrap';
//import { Link } from 'react-router-dom';
import './HeaderNavbar.css'
import ListOfferFavorite from "./Offer/ListOfferFavorite";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import UserService from "../services/UserService";



 class HeaderNavbar extends Component {


     constructor(props) {
         super(props)
         this.state = {
             username: UserService.getLoggedInUserName().username,
         };
     }


    render() {
        const isUserLoggedIn = UserService.getLoggedInUserName().username;
        console.log(isUserLoggedIn)
        return (
         <Navbar default collapseOnSelect expand="lg" bg="primary" variant="light">
            <Navbar.Brand  className="Navbar" variant="warning" href="/">
                <b className="we">We</b><b className="share">Share</b><b className="job">Job</b></Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>
               <Nav className="mr-auto">
                <Nav.Link className="navlink"  id="black"  href="/">Home</Nav.Link>
                <Nav.Link className="navlink" id="black" href="/signup">SignUp</Nav.Link>
                   {isUserLoggedIn && <Nav.Link className="navlink" id="black" href="/account">Account</Nav.Link>}
                    <Nav.Link className="navlink" id="black"  href="/listoffer">List Offers</Nav.Link>
                   <Switch>
                   {isUserLoggedIn && <Nav.Link className="navlink" id="black"   href={"/listOfferFavorite/" + this.state.username}>List Offers Favorite</Nav.Link> }
                   </Switch>
                   {!isUserLoggedIn && <Nav.Link className="navlink" id="black" href="/login">Login</Nav.Link>}
                   {isUserLoggedIn && <button className="navlink" id="red" onClick={UserService.logOut}>Logout</button>}
            </Nav>
                </Navbar.Collapse>
        </Navbar>
        )
    }
}
export default HeaderNavbar;