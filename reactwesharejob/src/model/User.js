export class User {
    constructor( username, password,token, id, roles, email, active) {
        this.username = username;
        this.password = password;
        this.token = token;
        this.id = id;
        this.email = email;
        this.active = active;
        this.roles =roles;
    }
}