package com.example.demo;

import com.example.demo.models.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.models.UserEntity;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.AccountService;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import java.util.Collections;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "AuthenticationApi", version = "1.0", description = "authentication APi"))
@EnableScheduling
@EnableConfigurationProperties
@EnableDiscoveryClient
public class AuthenticationApplication {

	
	@Autowired
	private AccountService accountService;

	@Autowired 
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	
	public static void main(String[] args) {
		SpringApplication.run(AuthenticationApplication.class, args);
	
	}
	

	@Bean
	CommandLineRunner init(UserRepository userRepository){ return args->{


		  UserEntity user = new UserEntity(2L, "admin", "email",passwordEncoder.encode("1234"), Role.USER, false);
		System.out.println(Role.USER);
		  userRepository.save(user);
		  };
		  
	}

		  
		  /*@Bean
		   CommandLineRunner init(AccountService accountService){ return args->{
		    
			   accountService.saveRoleEntity(new RoleEntity(null,"USER"));
		       accountService.saveRoleEntity(new RoleEntity(null,"ADMIN"));
		       accountService.saveUserEntity(new UserEntity(null, "user", "email",passwordEncoder.encode("1234"), null,1));
		       accountService.saveUserEntity(new UserEntity(null, "admin", "email",passwordEncoder.encode("1234"), null,1));
		       accountService.addRoleToUser("admin","ADMIN");

		        };

		  }*/

}
