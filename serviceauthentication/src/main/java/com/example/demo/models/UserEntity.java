package com.example.demo.models;

import java.util.Collection;
import java.util.Collections;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name="USERENTITY")
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id_pk")
	private Long id;
	@Column(name = "email")
	private String email;
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(nullable =false)
	private Boolean active;

	@Transient
	private String token;


/*	@ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
	@Cascade(value = org.hibernate.annotations.CascadeType.REMOVE)
	@JoinTable(
			indexes = {@Index(name = "INDEX_USER_ROLE", columnList = "user_id_pk")},
			name = "roles",
			joinColumns = @JoinColumn(name = "user_id_pk")
	)
	@Column(name = "roles", nullable = false)
	@Enumerated(EnumType.STRING)
	//private Collection<Role> roles;
	Role roles;*/


	@Enumerated(EnumType.STRING)
	@Column(name="role")
	private Role role;


/*	//revoir l'accessibilité
	public UserEntity() {
		this.roles = Collections.singletonList(Role.USER);
	}*/

	
	public UserEntity(Long id, String username, String email, String password,final Role role, Boolean active ) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.role = role;
		this.active = active;
	}

	public UserEntity() {

	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}
	
	@JsonSetter
	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Boolean getActive() {
		return active;
	}

	public boolean setActive(Boolean active) {
		this.active = active;
		return false;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}




	@Override
	public String toString() {
		return "UserEntity [id=" + id + ", email=" + email + ", username=" + username + ", password=" + password + ", active=" + active
				+ ", roles=" + role + "]";
	}
}
