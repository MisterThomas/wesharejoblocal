package com.example.demo.services;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import com.example.demo.models.Role;
import com.example.demo.models.UserEntity;

import com.example.demo.repositories.UserRepository;
import org.apache.logging.log4j.core.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;


@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceTest {

    @InjectMocks
    private AccountService accountService;

    @Mock
    private UserRepository userEntityRepository;

    @Mock
    private BCryptPasswordEncoder bcryptPasswordEncoder;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }

    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des test: {@} ms", duration));
    }



    @Test
    public void testSaveUser() throws Exception {
        //UserEntity user = Mockito.mock(UserEntity.class);
        UserEntity user = new UserEntity(1L, "test","test.com","test", Role.USER, false);
        Mockito.when(bcryptPasswordEncoder.encode(user.getPassword())).thenReturn("test");
        String hash = bcryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(hash);

        Mockito.when(userEntityRepository.save(user)).thenReturn(user);
        UserEntity result = accountService.saveUserEntity(user);
        Assertions.assertTrue(user.getUsername().equals(result.getUsername()));
    }

}
