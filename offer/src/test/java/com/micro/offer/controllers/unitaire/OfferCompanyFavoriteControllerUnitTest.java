package com.micro.offer.controllers.unitaire;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.micro.offer.controllers.OfferCompanyFavoriteController;
import com.micro.offer.model.Offer;
import com.micro.offer.model.OfferCompany;
import com.micro.offer.model.OfferCompanyFavorite;
import com.micro.offer.services.OfferCompanyFavoriteService;
import com.micro.offer.services.OfferCompanyService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(OfferCompanyFavoriteController.class)
public class OfferCompanyFavoriteControllerUnitTest {



   @Autowired
   MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

  // private ObjectMapper objectMapper = new ObjectMapper();


    @MockBean
    private OfferCompanyService offerCompanyService;

    @MockBean
    private OfferCompanyFavoriteService offerCompanyFavoriteService;




    @Test
  public  void getAllOfferCompanyFavoriteTest() throws Exception {
        List<OfferCompanyFavorite> offerCompanyFavoritesList = new ArrayList<OfferCompanyFavorite>();
        Date date = new Date();
        offerCompanyFavoritesList.add(new OfferCompanyFavorite(1L,  "user",date,  false));
        offerCompanyFavoritesList.add(new OfferCompanyFavorite(1L, "user",date,  false));
        given(offerCompanyFavoriteService.getOfferCompanyFavoriteAll()).willReturn(offerCompanyFavoritesList);

        mockMvc.perform(get("/api/offerCompanyFavorites/listOffersCompanyFavorites")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)));
    }


    @Test
    public  void getOfferCompanyFavoritebyIdTest() throws Exception {
        List<OfferCompanyFavorite> offerFavoriteList = new ArrayList<OfferCompanyFavorite>();
        Date date = new Date();
        Optional<OfferCompanyFavorite> offerCompanyFavorite = Optional.of(new OfferCompanyFavorite(1L, "user", date, false));
        offerFavoriteList.add(new OfferCompanyFavorite(1L, "user", date, false));
        offerFavoriteList.add(new OfferCompanyFavorite(1L, "user", date, false));
        given(offerCompanyFavoriteService.getOfferCompanyFavoriteById(offerFavoriteList.get(1).getId())).willReturn(offerCompanyFavorite);

        mockMvc.perform(get("/api/offerCompanyFavorites/listOffersCompanyFavorites/" + offerFavoriteList.get(1).getId())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public  void getOfferFavoriteUsernameTest() throws Exception {
        List<OfferCompanyFavorite> offerCompanyFavoriteList = new ArrayList<OfferCompanyFavorite>();
        Date date = new Date();
        offerCompanyFavoriteList.add(new OfferCompanyFavorite(1L, "user", date, false));
        offerCompanyFavoriteList.add(new OfferCompanyFavorite(1L, "user", date, false));
        given(offerCompanyFavoriteService.getOfferCompanyFavoriteByUsername(offerCompanyFavoriteList.get(1).getUsername())).willReturn(offerCompanyFavoriteList);

        mockMvc.perform(get("/api/offerCompanyFavorites/listOffersCompanyFavorite/username/{" + offerCompanyFavoriteList.get(1).getUsername())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public  void postOfferCompanyFavoriteTest() throws Exception {
        Date date = new Date();
        OfferCompany offerCompany = new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wesharejob.co", false);
       OfferCompanyFavorite offerCompanyFavorite = new OfferCompanyFavorite(1L, "user",date,  false);
       given(offerCompanyService.getOfferCompanyById(1L)).willReturn(Optional.of(offerCompany));
       offerCompanyFavorite.setOffercompany(offerCompany);
        given(offerCompanyFavoriteService.saveOfferCompanyFavorite(offerCompanyFavorite)).willReturn(offerCompanyFavorite);

        mockMvc.perform(post("/api/offerCompanyFavorites/listOffersCompany/" + offerCompanyFavorite.getOffercompany().getId() + "/add-OfferCompanyFavorite")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offerCompanyFavorite))
                ).andDo(print()).andExpect(status().is2xxSuccessful());
    }



   @Test
    public  void deleteOfferCompanyFavoriteTest() throws Exception {
        Date date = new Date();
        OfferCompanyFavorite offerCompanyFavorite = new OfferCompanyFavorite(1L, "user",date,  false);
       long id = 1;
       given(offerCompanyFavoriteService.deleteOfferCompanyFavoriteById(offerCompanyFavorite.getId())).willReturn(null);
        mockMvc.perform(delete("/api/offerCompanyFavorites/listOffersCompanyFavorite/delete/" + id)
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }


}
