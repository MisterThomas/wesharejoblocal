package com.micro.offer.controllers.unitaire;




import com.fasterxml.jackson.databind.ObjectMapper;
import com.micro.offer.controllers.OfferCompanyController;
import com.micro.offer.model.Offer;
import com.micro.offer.model.OfferCompany;
import com.micro.offer.repositories.OfferCompanyRepository;
import com.micro.offer.services.OfferCompanyService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(OfferCompanyController.class)
public class OfferControllerCompanyUnitTest {

    @Autowired
    MockMvc mockMvc;


  @Autowired
    private ObjectMapper objectMapper;

    @MockBean
     private OfferCompanyService offerService;

    @MockBean
    private OfferCompanyRepository offerCompanyRepository;


    @Test
  public  void getAllOfferCompanyTest() throws Exception {
        List<OfferCompany> offerCompanyList = new ArrayList<OfferCompany>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerCompanyList.add(new OfferCompany(numero, "wesharjob", "premier post", "lorem ipsum", "paris", date, "thomas","wesharejob", false));
        offerCompanyList.add(new OfferCompany(numero2, "wesharjob", "premier post", "lorem ipsum", "marseille", date, "thomas","wesharejob", false));
        given(offerService.getAllOfferCompany()).willReturn(offerCompanyList);

        mockMvc.perform(get("/api/offerCompany/listOfferCompanies")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)));
    }


    @Test
    public  void postOfferCompanyTest() throws Exception {
        Date date = new Date();
        long numero = 1;
        OfferCompany offerCompany = new OfferCompany(numero, "wesharjob", "premier post", "lorem ipsum", "paris", date, "thomas","wesharejob", false);

        given(offerService.saveOfferCompany(offerCompany)).willReturn(offerCompany);

        mockMvc.perform(post("/api/offerCompany/listOffersCompanies/post")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offerCompany))
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }

    @Test
    public  void rechercheOfferCompanyTest() throws Exception {

        List<OfferCompany> offerCompanyList = new ArrayList<OfferCompany>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerCompanyList.add(new OfferCompany(numero, "wesharjob", "premier post", "lorem ipsum", "paris", date, "thomas","wesharejob", false));
        offerCompanyList.add(new OfferCompany(numero2, "wesharjob", "premier post", "lorem ipsum", "marseille", date, "thomas","wesharejob", false));

        given(offerService.getOfferByCompanyOrPostOrCity(offerCompanyList.get(1).getCompany(),offerCompanyList.get(1).getPost(), offerCompanyList.get(1).getCity())).willReturn(offerCompanyList);

        mockMvc.perform(get("/api/offerCompany/listOffersCompanies/search")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }


    @Test
    public  void updateOfferCompanyTest() throws Exception {
        long id = 1;
        Date date = new Date();
        OfferCompany offerCompany = new OfferCompany(id, "wesharjob", "premier post", "lorem ipsum", "paris", date, "thomas","wesharejob", false);
        given(offerCompanyRepository.findById(offerCompany.getId())).willReturn(Optional.of(offerCompany));
        offerCompany.setPost("deuxieme post");
        given(offerService.updateOfferCompany(offerCompany)).willReturn(offerCompany);
        mockMvc.perform(put("/api/offerCompany//listOffersCompanies/update/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offerCompany)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }


    @Test
    public  void deleteOfferCompanyTest() throws Exception {
        Date date = new Date();
        long numero = 1;
        OfferCompany offerCompany = new OfferCompany(numero, "wesharjob", "premier post", "lorem ipsum", "paris", date, "thomas","wesharejob", false);
        given(offerService.deleteOfferCompanyById(offerCompany.getId())).willReturn(null);
        long id = 1;
        mockMvc.perform(delete("/api/offerCompany//listOffersCompanies/delete/" + id)
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }


/*
    @Test
    public  void getOfferUsernameTest() throws Exception {
        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        given(offerService.getOfferByUsername(offerList.get(1).getUsername())).willReturn(offerList);

        mockMvc.perform(get("/api/offers/listOffers/username/" + offerList.get(1).getUsername())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }


    @Test
    public  void getOfferbyIdTest() throws Exception {
        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        Optional<Offer> offerId = Optional.of(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        given(offerService.getOfferById(offerList.get(1).getId())).willReturn(offerId);

        mockMvc.perform(get("/api/offers/listOffers/id/" + offerList.get(1).getId())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }*/

/*

    @Test
    public  void postOfferTest() throws Exception {
        Date date = new Date();
        long numero = 1;
       Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");

        given(offerService.saveOffer(offer)).willReturn(offer);

        mockMvc.perform(post("/api/offers/listOffers/post")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offer))
        ).andDo(print()).andExpect(status().isCreated());
    }



    @Test
    public  void rechercheOfferTest() throws Exception {

        List<Offer> offerList = new ArrayList<Offer>();
        Date date = new Date();
        long numero = 1;
        long numero2 = 2;
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));


        given(offerService.getOfferByCompanyOrPostOrCity(offerList.get(1).getCompany(),offerList.get(1).getPost(), offerList.get(1).getCity())).willReturn(offerList);

        mockMvc.perform(get("/api/offers/listOffers/search")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }


    @Test
    public  void updateOfferTest() throws Exception {
        long id = 1;
        Date date = new Date();
        Offer offer = new Offer(id, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        given(offerRepository.findById(offer.getId())).willReturn(Optional.of(offer));
        offer.setPost("deuxieme post");
        given(offerService.updateOffer(offer)).willReturn(offer);
        mockMvc.perform(put("/api/offers/listOffers/update/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offer)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }


   @Test
    public  void deleteOfferTest() throws Exception {
        Date date = new Date();
        long numero = 1;
        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        given(offerService.deleteOfferById(offer.getId())).willReturn(null);
        long id = 1;
        mockMvc.perform(delete("/api/offers/listOffers/delete/" + id)
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().is2xxSuccessful());
    }*/

}
