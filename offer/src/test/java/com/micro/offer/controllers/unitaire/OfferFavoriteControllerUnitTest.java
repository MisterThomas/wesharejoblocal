package com.micro.offer.controllers.unitaire;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.micro.offer.controllers.OfferFavoriteController;
import com.micro.offer.model.Offer;
import com.micro.offer.model.OfferFavorite;
import com.micro.offer.services.OfferFavoriteService;
import com.micro.offer.services.OfferService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(OfferFavoriteController.class)
public class OfferFavoriteControllerUnitTest {

           @Autowired
           MockMvc mockMvc;

             @Autowired
            private ObjectMapper objectMapper;

             @MockBean
             private OfferService offerService;


             @MockBean
            private OfferFavoriteService offerFavoriteService;

    @Test
    public void getAllOfferFavoriteTest() throws Exception {
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        Date date = new Date();

        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        offerFavoriteList.add(new OfferFavorite(2L, "user", date, "23/04/2021", false));
        given(offerFavoriteService.getOfferFavoriteByAll()).willReturn(offerFavoriteList);

        mockMvc.perform(get("/api/offerFavorites/listOffersFavorites")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    public  void getOfferFavoritebyIdTest() throws Exception {
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        Date date = new Date();
        Optional<OfferFavorite> offerFavorite = Optional.of(new OfferFavorite(1L, "user", date,"23/04/2021", false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021",false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        given(offerFavoriteService.getOfferFavoriteById(offerFavoriteList.get(1).getId())).willReturn(offerFavorite);

        mockMvc.perform(get("/api/offerFavorites//listOffersFavorites/" + offerFavoriteList.get(1).getId())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public  void getOfferFavoriteUsernameTest() throws Exception {
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        Date date = new Date();
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021",false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        given(offerFavoriteService.getOfferFavoriteByUsername(offerFavoriteList.get(1).getUsername())).willReturn(offerFavoriteList);

        mockMvc.perform(get("/api/offerFavorites/listOffersFavorites/username/" + offerFavoriteList.get(1).getUsername())
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void postOfferFavoriteTest() throws Exception {
        Date date = new Date();
        long numero = 1;

        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        OfferFavorite offerFavorite = new OfferFavorite(1L, "user", date,"23/04/2021", false);
        given(offerService.getOfferById(1L)).willReturn(Optional.of(offer));
        offerFavorite.setOffer(offer);
        given(offerFavoriteService.saveOfferFavorite(offerFavorite)).willReturn(offerFavorite);



        mockMvc.perform(post("/api/offerFavorites/listOffers/" + offerFavorite.getOffer().getId() + "/add-offerFavorite")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offerFavorite))
        ).andDo(print()).andExpect(status().isOk());
    }



    @Test
    public void deleteOfferFavoriteTest() throws Exception {
        Date date = new Date();
        long numero =1;
        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        OfferFavorite offerFavorite = new OfferFavorite(1L, "user", date,"23/04/2021", false);
        offerFavorite.setOffer(offer);
        given(offerFavoriteService.deleteOfferFavoriteById(offerFavorite.getId())).willReturn(null);
        long id = 1;
        mockMvc.perform(delete("/api/offerFavorites/listOffersFavorites/delete/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(offer))
        ).andDo(print()).andExpect(status().is2xxSuccessful());

    }
}
