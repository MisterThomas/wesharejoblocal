package com.micro.offer.controllers.integration;

/*
import com.fasterxml.jackson.databind.ObjectMapper;
import com.micro.offer.OfferApplication;
import com.micro.offer.model.Offer;
import com.micro.offer.repositories.OfferRepository;
import com.micro.offer.services.OfferService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { OfferApplication.class })
@WebAppConfiguration*/
public class OfferControllerTest {

/*

   private static final ObjectMapper om = new ObjectMapper();


    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferRepository offerRepository;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port + "/api/offers";
    }

    @Test
    public void contextLoads() {
    }

    @org.junit.jupiter.api.Test
    public void testGetAllOffer() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/listOffers",
                HttpMethod.GET, entity, String.class);

        Assert.assertNotNull(response.getBody());
    }


    @Test
    public void testRechercheOffer() {
        Date date = new Date();
        long numero = 1;
        Offer offer = new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        ResponseEntity<String> responseEntity = this.restTemplate
                .postForEntity(getRootUrl() + "/listOffers/search", offer, String.class);
        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testAddOffer() {
        Date date = new Date();
        Offer offer = new Offer(1L, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        ResponseEntity<String> responseEntity = this.restTemplate
                .postForEntity(getRootUrl() + "/listOffers/post", offer, String.class);
        Assert.assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testUpdatedOffer() {
        int id = 1;
        Offer offer = restTemplate.getForObject(getRootUrl() + "/listOffers/update/" + id, Offer.class);
        offer.setPost("deuxieme post");
        restTemplate.put(getRootUrl() + "/listOffers/update/" + id, offer);
        Offer updatedOffer = restTemplate.getForObject(getRootUrl() + "/listOffers/update/" + id, Offer.class);
        Assert.assertNotNull(updatedOffer);
    }




    @Test
    public void testDeleteOffer() {
        int id = 1;
        Offer offer = restTemplate.getForObject(getRootUrl() + "/listOffers/delete/" + id, Offer.class);
        Assert.assertNotNull(offer);
        restTemplate.delete(getRootUrl() + "/listOffers/delete/" + id);
        try {
            offer = restTemplate.getForObject(getRootUrl() + "/listOffers/delete/" + id, Offer.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }


    @Test
    public void testGetOfferByUsername() {
        String username = "thomas";
        Offer offer = restTemplate.getForObject(getRootUrl() + "/listOffers/username/" + username, Offer.class);
        System.out.println(offer.getCompany());
        Assert.assertNotNull(offer);
    }


    @Test
    public void testGetOfferById() {
        Offer offer = restTemplate.getForObject(getRootUrl() + "/listOffers/id/" + 1, Offer.class);
//        System.out.println(offer.getCompany());
        Assert.assertNotNull(offer);
    }

*/


}

