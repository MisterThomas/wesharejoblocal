package com.micro.offer.services;


import com.micro.offer.model.OfferCompanyFavorite;
import com.micro.offer.repositories.OfferCompanyFavoriteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@RunWith(MockitoJUnitRunner.class)
public class OfferCompanyFavoriteServiceTest {

    @Mock
    private OfferCompanyFavoriteRepository offerCompanyFavoriteRepository;

    @InjectMocks
    private OfferCompanyFavoriteService offerCompanyFavoriteService;

    private static Instant startedAt;


    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }



    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.printf("Durée des test: %s ms\n", duration);
    }


    @Test
    public void testGetAllOfferCompanyFavorite(){

        Date date = new Date();
        List<OfferCompanyFavorite> offerCompanyFavoriteList = new ArrayList<OfferCompanyFavorite>();
        offerCompanyFavoriteList.add(new OfferCompanyFavorite(1L, "user", date,  false));
        offerCompanyFavoriteList.add(new OfferCompanyFavorite(1L, "user", date,  false));
        offerCompanyFavoriteList.add(new OfferCompanyFavorite(1L, "user", date,  false));

        when(offerCompanyFavoriteRepository.findAll()).thenReturn(offerCompanyFavoriteList);

        List<OfferCompanyFavorite> result = offerCompanyFavoriteService.getOfferCompanyFavoriteAll();
        assertEquals(3, result.size());
    }



    @Test
    public void testGetOfferCompanyFavoriteById(){
        Date date = new Date();
        Date date1 = new Date();
        long numero1 = 1;

        Optional<OfferCompanyFavorite> getOfferCompanyFavorite = Optional.of(new OfferCompanyFavorite(1L, "user", date,  false));
        when(offerCompanyFavoriteRepository.findById(numero1)).thenReturn(getOfferCompanyFavorite);
        Optional<OfferCompanyFavorite> result = offerCompanyFavoriteService.getOfferCompanyFavoriteById(numero1);
        assertEquals(Optional.of(numero1), Optional.ofNullable(result.get().getId()));
        assertEquals(false, result.get().getIs_active());
        assertEquals("user", result.get().getUsername());
    }



    @Test
    public void testGetOfferCompanyFavoriteByUsername(){
        Date date = new Date();
        String name = "user";
        List<OfferCompanyFavorite> offerCompanyFavoritesList = new ArrayList<OfferCompanyFavorite>();
        offerCompanyFavoritesList.add(new OfferCompanyFavorite(1L, "user", date,  false));
        when(offerCompanyFavoriteRepository.findByUsername("user")).thenReturn(offerCompanyFavoritesList);
        List<OfferCompanyFavorite> result = offerCompanyFavoriteService.getOfferCompanyFavoriteByUsername(name);
        assertEquals("user", result.listIterator().next().getUsername());
    }



    @Test
    public void testGetOfferCompanyFavoriteByDate(){
        Date date = new Date();
        Date date1 = new Date();
        String name = "user";
        List<OfferCompanyFavorite> offerCompanyFavoritesList = new ArrayList<OfferCompanyFavorite>();
        offerCompanyFavoritesList.add(new OfferCompanyFavorite(1L, "user", date,  false));
        when(offerCompanyFavoriteRepository.findALLByAndDateScrapOffer(date)).thenReturn(offerCompanyFavoritesList);
        List<OfferCompanyFavorite> result = offerCompanyFavoriteService.OfferCompanyFavoritebyDate(date1);
        assertEquals(date, result.listIterator().next().getDate_scrap_offer());
    }


    @Test
    public void testGetSaveOfferCompanyFavorite(){
        Date date = new Date();
        Date date1 = new Date();
        OfferCompanyFavorite offerCompanyFavorite = new OfferCompanyFavorite(1L, "user", date,  false);
        when(offerCompanyFavoriteRepository.save(offerCompanyFavorite)).thenReturn(offerCompanyFavorite);
        OfferCompanyFavorite result = offerCompanyFavoriteService.saveOfferCompanyFavorite(offerCompanyFavorite);
        assertEquals(Optional.of(1L), Optional.ofNullable(result.getId()));
        assertEquals(false, result.getIs_active());
        assertEquals("user", result.getUsername());
    }


    @Test
    public void testGetUpdateOfferCompanyFavorite(){
        Date date = new Date();
        Date date1 = new Date();
        OfferCompanyFavorite offerCompanyFavorite = new OfferCompanyFavorite(1L, "user", date,  false);
        when(offerCompanyFavoriteRepository.save(offerCompanyFavorite)).thenReturn(offerCompanyFavorite);
        OfferCompanyFavorite result1 = offerCompanyFavoriteService.saveOfferCompanyFavorite(offerCompanyFavorite);
        offerCompanyFavorite.setUsername("user2");
        when(offerCompanyFavoriteRepository.save(offerCompanyFavorite)).thenReturn(offerCompanyFavorite);
        OfferCompanyFavorite result = offerCompanyFavoriteService.updateOfferCompanyFavorite(offerCompanyFavorite);
        assertEquals("user2", result.getUsername());
    }

    @Test
    public void testRemoveOfferCompanyFavorite(){
        Date date = new Date();
        OfferCompanyFavorite offerCompanyFavorite = new OfferCompanyFavorite(1L, "user", date,  false);
        offerCompanyFavoriteService.deleteOfferCompanyFavorite(offerCompanyFavorite.getId());
        verify(offerCompanyFavoriteRepository, times(0)).delete(offerCompanyFavorite);
    }




}
