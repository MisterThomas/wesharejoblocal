package com.micro.offer.services;



import com.micro.offer.model.OfferFavorite;
import com.micro.offer.repositories.OfferFavoriteRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@RunWith(MockitoJUnitRunner.class)
public class OfferFavoriteServiceTest {

    @Mock
    private OfferFavoriteRepository offerFavoriteRepository;

    @InjectMocks
    private  OfferFavoriteService offerFavoriteService;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }



    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.printf("Durée des test: %s ms\n", duration);
    }

    @Test
    public void testGetAllOfferFavorite(){

        Date date = new Date();
        List<OfferFavorite> offerFavoriteList = new ArrayList<OfferFavorite>();
        offerFavoriteList.add(new OfferFavorite(1L, "user", date,"23/04/2021",  false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date,"23/04/2021",  false));
        offerFavoriteList.add(new OfferFavorite(1L, "user", date,"23/04/2021",  false));

        when(offerFavoriteRepository.findAll()).thenReturn(offerFavoriteList);

        List<OfferFavorite> result = offerFavoriteService.getOfferFavoriteByAll();
        assertEquals(3, result.size());
    }



    @Test
    public void testGetOfferFavoriteById(){
        Date date = new Date();
        Date date1 = new Date();
        long numero = 1;
        long numero1 = 1;

        Optional<OfferFavorite> getOfferFavorite = Optional.of(new OfferFavorite(1L, "user", date,"23/04/2021" , false));
        when(offerFavoriteRepository.findById(numero1)).thenReturn(getOfferFavorite);
        Optional<OfferFavorite> result = offerFavoriteService.getOfferFavoriteById(numero1);
        assertEquals(Optional.of(numero1), Optional.ofNullable(result.get().getId()));
        assertEquals(false, result.get().getIs_active());
        assertEquals("user", result.get().getUsername());
    }



    @Test
    public void testGetOfferFavoriteByUsername(){
        Date date = new Date();
        String name = "user";
        List<OfferFavorite> offerFavoritesList = new ArrayList<OfferFavorite>();
        offerFavoritesList.add(new OfferFavorite(1L, "user", date, "23/04/2021", false));
        when(offerFavoriteRepository.findByUsername("user")).thenReturn(offerFavoritesList);
        List<OfferFavorite> result = offerFavoriteService.getOfferFavoriteByUsername(name);
        assertEquals("user", result.listIterator().next().getUsername());
    }



    @Test
    public void testGetOfferFavoriteByDate(){
        Date date = new Date();
        Date date1 = new Date();
        String name = "user";
        List<OfferFavorite> offerFavoritesList = new ArrayList<OfferFavorite>();
        offerFavoritesList.add(new OfferFavorite(1L, "user", date,"23/04/2021",  false));
        when(offerFavoriteRepository.findALLByAndDateScrapOffer("23/04/2021")).thenReturn(offerFavoritesList);
        List<OfferFavorite> result = offerFavoriteService.OfferFavoritebyDate("23/04/2021");
        assertEquals(date, result.listIterator().next().getDateBatch());
    }


    @Test
    public void testGetSaveOfferFavorite(){
        Date date = new Date();
        Date date1 = new Date();
        OfferFavorite offerFavorite = new OfferFavorite(1L, "user", date, "23/04/2021", false);
        when(offerFavoriteRepository.save(offerFavorite)).thenReturn(offerFavorite);
        OfferFavorite result = offerFavoriteService.saveOfferFavorite(offerFavorite);
        assertEquals(Optional.of(1L), Optional.ofNullable(result.getId()));
        assertEquals(false, result.getIs_active());
        assertEquals("user", result.getUsername());
    }


    @Test
    public void testGetUpdateOfferCompanyFavorite(){
        Date date = new Date();
        Date date1 = new Date();
        OfferFavorite offerFavorite = new OfferFavorite(1L, "user", date,"23/04/2021",  false);
        when(offerFavoriteRepository.save(offerFavorite)).thenReturn(offerFavorite);
        OfferFavorite result1 = offerFavoriteService.saveOfferFavorite(offerFavorite);
        offerFavorite.setUsername("user2");
        when(offerFavoriteRepository.save(offerFavorite)).thenReturn(offerFavorite);
        OfferFavorite result = offerFavoriteService.updateOfferFavorite(offerFavorite);
        assertEquals("user2", result.getUsername());
    }

    @Test
    public void testRemoveOfferCompanyFavorite(){
        Date date = new Date();
        OfferFavorite offerCompanyFavorite = new OfferFavorite(1L, "user", date, "23/04/2021", false);
        offerFavoriteService.deleteOfferFavorite(offerCompanyFavorite.getId());
        verify(offerFavoriteRepository, times(0)).delete(offerCompanyFavorite);
    }
}
