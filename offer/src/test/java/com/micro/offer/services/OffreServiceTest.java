package com.micro.offer.services;

import com.micro.offer.model.Offer;
import com.micro.offer.repositories.OfferRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@RunWith(MockitoJUnitRunner.class)
public class OffreServiceTest {


    @Mock
    private  OfferRepository offerRepository;

    @InjectMocks
    private  OfferService offerService;

    private static Instant startedAt;

    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }


    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }



    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.printf("Durée des test: %s ms\n", duration);
    }


    @Test
    public void testGetAllOffer(){

        Date date = new Date();
        List<Offer> offerList = new ArrayList<Offer>();
        offerList.add(new Offer(1L,"wesharjob", "premier post","la ville que tu veux", "wesharejob.co",  false, date,"23/04/2021",  "user"));
        offerList.add(new Offer(1L,"wesharjob", "premier post","la ville que tu veux", "wesharejob.co",  false, date,"23/04/2021",   "user"));
        offerList.add(new Offer(1L,"wesharjob", "premier post","la ville que tu veux", "wesharejob.co",  false, date,"23/04/2021",   "user"));

        when(offerRepository.findAll()).thenReturn(offerList);

        List<Offer> result = offerService.getAllOffer();
        assertEquals(3, result.size());
    }



    @Test
    public void testGetOfferById(){
        Date date = new Date();
        Date date1 = new Date();
        long numero = 1;
        long numero1 = 1;

        Optional<Offer> getOffer = Optional.of(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021",  "user"));
        when(offerRepository.findById(numero1)).thenReturn(getOffer);
        Optional<Offer> result = offerService.getOfferById(numero1);
        assertEquals(Optional.of(numero1), Optional.ofNullable(result.get().getId()));
        assertEquals("wesharjob", result.get().getCompany());
        assertEquals("premier post", result.get().getPost());
        assertEquals("la ville que tu veux", result.get().getCity());
        assertEquals("wesharejob.co", result.get().getUrl());
        assertEquals(false, result.get().getIs_active());
        assertEquals(date1, result.get().getDateBatch());
        assertEquals("user", result.get().getUsername());
    }



    @Test
    public void testGetOfferByUsername(){
        Date date = new Date();
        String name = "user";
        long numero = 1;
        List<Offer> offerList = new ArrayList<Offer>();
                offerList.add(new Offer( numero,"wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,  "23/04/2021",  "user"));
        when(offerRepository.findByUsername("user")).thenReturn(offerList);
        List<Offer> result = offerService.getOfferByUsername(name);
        assertEquals("user", result.listIterator().next().getUsername());
    }



    @Test
    public void testGetOfferByDate(){
        Date date = new Date();
        Date date1 = new Date();
        String name = "user";
        long numero = 1;
        List<Offer> offerList = new ArrayList<Offer>();
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        when(offerRepository.findALLByAndDateScrapOffer("23/04/2021")).thenReturn(offerList);
        List<Offer> result = offerService.OfferbyDate("23/04/2021");
        assertEquals(date1, result.listIterator().next().getDateBatch());
    }


    @Test
    public void testGetOfferByCompanyOrPostOrCity(){
        Date date = new Date();
        String company = "wesharjob";
        List<Offer> offerList = new ArrayList<Offer>();
        long numero = 1;
        long numero2 = 2;
        offerList.add(new Offer(numero, "wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        offerList.add(new Offer(numero2, "wesharjob1", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user"));
        when(offerRepository.findByCompanyOrCityOrPost("wesharjob", "", "")).thenReturn(offerList);
        List<Offer> result = offerService.getOfferByCompanyOrPostOrCity("wesharjob", "", "");
        assertEquals("wesharjob", result.listIterator().next().getCompany());
    }


    @Test
    public void testGetSaveOffer(){
        Date date = new Date();
        Date date1 = new Date();

        Offer offer = new Offer(1l,"wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date, "23/04/2021","user");
        when(offerRepository.save(offer)).thenReturn(offer);
        Offer result = offerService.saveOffer(offer);
        assertEquals(Optional.of(1L), Optional.ofNullable(result.getId()));
        assertEquals("wesharjob", result.getCompany());
        assertEquals("premier post", result.getPost());
        assertEquals("la ville que tu veux", result.getCity());
        assertEquals("wesharejob.co", result.getUrl());
        assertEquals(false, result.getIs_active());
        assertEquals(date1, result.getDateBatch());
        assertEquals("user", result.getUsername());
    }


    @Test
    public void testGetUpdateOffer(){
        Date date = new Date();
        Date date1 = new Date();
        Offer offer = new Offer(1l,"wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        when(offerRepository.save(offer)).thenReturn(offer);
        Offer result1 = offerService.saveOffer(offer);
        offer.setPost("deuxieme post");
        when(offerRepository.save(offer)).thenReturn(offer);
        Offer result = offerService.updateOffer(offer);
        assertEquals("deuxieme post", result.getPost());
    }

    @Test
    public void testRemoveOffer(){
        Date date = new Date();
        Offer offer = new Offer(1l,"wesharjob", "premier post", "la ville que tu veux", "wesharejob.co", false, date,"23/04/2021", "user");
        offerService.deleteOffer(offer.getId());
        verify(offerRepository, times(0)).delete(offer);
    }
}
