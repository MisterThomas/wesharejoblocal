package com.micro.offer.services;


import com.micro.offer.model.OfferCompany;
import com.micro.offer.repositories.OfferCompanyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;


import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@RunWith(MockitoJUnitRunner.class)
public class OfferCompanyServiceTest {


    @Mock
    private OfferCompanyRepository offerCompanyRepository;

    @InjectMocks
    private   OfferCompanyService offerCompanyService;

    private static Instant startedAt;


    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }


    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }


    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }



    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.printf("Durée des test: %s ms\n", duration);
    }


    @Test
    public void testGetAllOfferCompany(){

        Date date = new Date();
        List<OfferCompany> offerCompanyList = new ArrayList<OfferCompany>();
        offerCompanyList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wesharejob.co", false));
        offerCompanyList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wesharejob.co", false));
        offerCompanyList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wesharejob.co", false));

        when(offerCompanyRepository.findAll()).thenReturn(offerCompanyList);

        List<OfferCompany> result = offerCompanyService.getAllOfferCompany();
        assertEquals(3, result.size());
    }



    @Test
    public void testGetOfferCompanyById(){
        Date date = new Date();
        Date date1 = new Date();
        long numero = 1;
        long numero1 = 1;

        Optional<OfferCompany> getOfferCompany = Optional.of(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wesharejob.co", false));
        when(offerCompanyRepository.findById(numero1)).thenReturn(getOfferCompany);
        Optional<OfferCompany> result = offerCompanyService.getOfferCompanyById(numero1);
        assertEquals(Optional.of(numero1), Optional.ofNullable(result.get().getId()));
        assertEquals("wesharjob", result.get().getCompany());
        assertEquals("premier post", result.get().getPost());
        assertEquals("lorem ipsum", result.get().getDescription());
        assertEquals("la ville que tu veux", result.get().getCity());
        assertEquals("wesharejob.co", result.get().getUrl());
        assertEquals(false, result.get().getIs_active());
        assertEquals(date1, result.get().getDate_offer());
        assertEquals("user", result.get().getUsername());
    }



    @Test
    public void testGetOfferCompanyByUsername(){
        Date date = new Date();
        String name = "user";
        List<OfferCompany> offerCompanyList = new ArrayList<OfferCompany>();
        offerCompanyList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wersharejob.co", false));
        when(offerCompanyRepository.findByUsername("user")).thenReturn(offerCompanyList);
        List<OfferCompany> result = offerCompanyService.getOfferByUsername(name);
        assertEquals("user", result.listIterator().next().getUsername());
    }



    @Test
    public void testGetOfferCompanyByDate(){
        Date date = new Date();
        Date date1 = new Date();
        String name = "user";
        List<OfferCompany> offerCompanyList = new ArrayList<OfferCompany>();
        offerCompanyList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wersharejob.co", false));
        when(offerCompanyRepository.findALLByAndDateScrapOffer(date)).thenReturn(offerCompanyList);
        List<OfferCompany> result = offerCompanyService.OfferCompanybyDate(date1);
        assertEquals(date, result.listIterator().next().getDate_offer());
    }


    @Test
    public void testGetOfferCompanyByCompanyOrPostOrCity(){
        Date date = new Date();
        String company = "wesharjob";
        List<OfferCompany> offerList = new ArrayList<OfferCompany>();
        offerList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wersharejob.co", false));
        offerList.add(new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wersharejob.co", false));
        when(offerCompanyRepository.findByCompanyOrCityOrPost("wesharjob", "", "")).thenReturn(offerList);
        List<OfferCompany> result = offerCompanyService.getOfferByCompanyOrPostOrCity("wesharjob", "", "");
        assertEquals("wesharjob", result.listIterator().next().getCompany());
    }


    @Test
    public void testGetSaveOfferCompany(){
        Date date = new Date();
        Date date1 = new Date();
        OfferCompany offerCompany = new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wesharejob.co", false);
        when(offerCompanyRepository.save(offerCompany)).thenReturn(offerCompany);
        OfferCompany result = offerCompanyService.saveOfferCompany(offerCompany);
        assertEquals(Optional.of(1L), Optional.ofNullable(result.getId()));
        assertEquals("wesharjob", result.getCompany());
        assertEquals("premier post", result.getPost());
        assertEquals("lorem ipsum", result.getDescription());
        assertEquals("la ville que tu veux", result.getCity());
        assertEquals("wesharejob.co", result.getUrl());
        assertEquals(false, result.getIs_active());
        assertEquals(date1, result.getDate_offer());
        assertEquals("user", result.getUsername());
    }


    @Test
    public void testGetUpdateOfferCompany(){
        Date date = new Date();
        Date date1 = new Date();
        OfferCompany offerCompany = new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wersharejob.co", false);
        when(offerCompanyRepository.save(offerCompany)).thenReturn(offerCompany);
        OfferCompany result1 = offerCompanyService.saveOfferCompany(offerCompany);
        offerCompany.setPost("deuxieme post");
        when(offerCompanyRepository.save(offerCompany)).thenReturn(offerCompany);
        OfferCompany result = offerCompanyService.updateOfferCompany(offerCompany);
        assertEquals("deuxieme post", result.getPost());
    }

    @Test
    public void testRemoveOfferCompany(){
        Date date = new Date();
        OfferCompany offerCompany = new OfferCompany(1L, "wesharjob","premier post", "lorem ipsum", "la ville que tu veux", date, "user", "wersharejob.co", false);
        offerCompanyService.deleteOfferCompany(offerCompany.getId());
        verify(offerCompanyRepository, times(0)).delete(offerCompany);
    }
}
