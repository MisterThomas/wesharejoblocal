package com.micro.offer.model;


import org.apache.logging.log4j.core.Logger;
import org.junit.jupiter.api.*;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Tag("OfferFavoriteTest")
@DisplayName("Check OfferFavorite model")
public class OfferFavoriteTest {

   private OfferFavorite offerFavorite;

    private static Instant startedAt;

    //utiliser la classe LoggingExtension au lieu de System.out.println
    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }

    //Calcul de delai antre début et fin des tests
    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des test: {0} ms", duration));

    }

    //appel avant chaque test
    @BeforeEach
    public void initOffer() {
        offerFavorite = new OfferFavorite();
        System.out.println("Appel avant chaque test");
    }

    //appel après chaque test
    @AfterEach
    public void undefOffer() {
        System.out.println("Appel après chaque test");
        offerFavorite = null;
    }


    @Test
    public  void OfferFavoriteTest(){

        Offer offer = new Offer();

        OfferFavorite offerFavorite = new OfferFavorite();

        System.out.println("test");

        final long id = 1;
        final long id1 = 1;
        final String company1 = "wesharejob";
        final String post1 = "premier post";
        final String description1 = "description";
        final String city1 = "paris";
        final Date dateBatch = new Date();
        final String date_scrap_offer = "23/04/2021";

        final String username1 = "user";
        final String url1 = "www.wesharejob.com";
        final Boolean is_active1 = false;

        offer.setId(id1);
        offer.setCompany(company1);
        offer.setPost(post1);
        offer.setCity(city1);
        offer.setDateBatch(dateBatch);
        offer.setDate_scrap_offer(date_scrap_offer);
        offer.setUsername(username1);
        offer.setUrl(url1);
        offer.setIs_active(is_active1);




        offerFavorite.setId(id);
        offerFavorite.setUsername(username1);
        offerFavorite.setDateBatch(dateBatch);
        offerFavorite.setDate_scrap_offer(date_scrap_offer);
        offerFavorite.setIs_active(is_active1);
        offerFavorite.setOffer(offer);

        offerFavorite.getId();
        offerFavorite.getUsername();
        offerFavorite.getDateBatch();
        offerFavorite.getDate_scrap_offer();
        offerFavorite.getIs_active();
        offerFavorite.getOffer();
    }
}
