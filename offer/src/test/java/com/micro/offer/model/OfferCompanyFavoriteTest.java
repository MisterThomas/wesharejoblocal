package com.micro.offer.model;


import org.apache.logging.log4j.core.Logger;

import org.junit.jupiter.api.*;


import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Tag("OfferCompanyFavoriteTest")
@DisplayName("Check OfferCompanyFavorite model")
public class OfferCompanyFavoriteTest {

    private OfferCompanyFavorite offerCompanyFavorite;

    private static Instant startedAt;

    //utiliser la classe LoggingExtension au lieu de System.out.println
    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }

    //Calcul de delai antre début et fin des tests
    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des test: {0} ms", duration));

    }

    //appel avant chaque test
    @BeforeEach
    public void initOffer() {
        offerCompanyFavorite = new OfferCompanyFavorite();
        System.out.println("Appel avant chaque test");
    }

    //appel après chaque test
    @AfterEach
    public void undefOffer() {
        System.out.println("Appel après chaque test");
        offerCompanyFavorite = null;
    }

    @Test
    public  void OfferCompanyFavoriteTest(){

        OfferCompany offerCompany = new OfferCompany();

        OfferCompanyFavorite offerCompanyFavorite = new OfferCompanyFavorite();

        System.out.println("test");

        final long id = 1;
        final long id1 = 1;
        final String company1 = "wesharejob";
        final String post1 = "premier post";
        final String description1 = "description";
        final String city1 = "paris";
        final Date date_offer1 = new Date(01/02/2021);
        final String username1 = "user";
        final String url1 = "www.wesharejob.com";
        final Boolean is_active1 = false;

        offerCompany.setId(id1);
        offerCompany.setCompany(company1);
        offerCompany.setDescription(description1);
        offerCompany.setPost(post1);
        offerCompany.setCity(city1);
        offerCompany.setDate_offer(date_offer1);
        offerCompany.setUsername(username1);
        offerCompany.setUrl(url1);
        offerCompany.setIs_active(is_active1);


        offerCompanyFavorite.setId(id);
        offerCompanyFavorite.setUsername(username1);
        offerCompanyFavorite.setDate_scrap_offer(date_offer1);
        offerCompanyFavorite.setIs_active(is_active1);
        offerCompanyFavorite.setOffercompany(offerCompany);

        offerCompanyFavorite.getId();
        offerCompanyFavorite.getUsername();
        offerCompanyFavorite.getDate_scrap_offer();
        offerCompanyFavorite.getIs_active();
        offerCompanyFavorite.getOffercompany();

    }
}
