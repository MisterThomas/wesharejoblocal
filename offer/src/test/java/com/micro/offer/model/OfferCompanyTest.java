package com.micro.offer.model;



import org.apache.logging.log4j.core.Logger;
import org.junit.jupiter.api.*;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Tag("OfferCompanyTest")
@DisplayName("Check OfferCompanyTest model")
public class OfferCompanyTest {

   private OfferCompany offerCompany;

    private static Instant startedAt;

    //utiliser la classe LoggingExtension au lieu de System.out.println
    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }

    //Calcul de delai antre début et fin des tests
    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des test: {0} ms", duration));

    }

    //appel avant chaque test
    @BeforeEach
    public void initOffer() {
        offerCompany = new OfferCompany();
        System.out.println("Appel avant chaque test");
    }

    //appel après chaque test
    @AfterEach
    public void undefOffer() {
        System.out.println("Appel après chaque test");
        offerCompany = null;
    }


    @Test
    public void OfferCompanyTest(){

        OfferCompany offerCompany = new OfferCompany();

        System.out.println("test");

        final long id = 1;
        final String company = "wesharejob";
        final String post = "premier post";
        final String description = "description";
        final String city = "paris";
        final Date date_offer = new Date(01/02/2021);
        final String username = "user";
        final String url = "www.wesharejob.com";
        final Boolean is_active = false;

        offerCompany.setId(id);
        offerCompany.setCompany(company);
        offerCompany.setDescription(description);
        offerCompany.setPost(post);
        offerCompany.setCity(city);
        offerCompany.setDate_offer(date_offer);
        offerCompany.setUsername(username);
        offerCompany.setUrl(url);
        offerCompany.setIs_active(is_active);
        offerCompany.getCompany();
        offerCompany.getId();
        offerCompany.getDate_offer();
        offerCompany.getPost();
        offerCompany.getUsername();
        offerCompany.getDescription();
        offerCompany.getIs_active();
        offerCompany.getCity();
        offerCompany.getUrl();
        offerCompany.getIs_active();
    }
}
