package com.micro.offer.model;



import org.apache.logging.log4j.core.Logger;
import org.junit.jupiter.api.*;


import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Tag("OfferTest")
@DisplayName("Check Offer model")
public class OfferTest {

    private Offer offer;

    private static Instant startedAt;

    //utiliser la classe LoggingExtension au lieu de System.out.println
    private Logger logger;

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    //appel avant tout les tests pour demarrer le chrono
    @BeforeAll
    public static void initStartingTime() {
        System.out.println("Appel avant tout les tests");
        startedAt = Instant.now();
    }

    //Calcul de delai antre début et fin des tests
    @AfterAll
    public static void testDuration() {
        System.out.println("Appel après tout les tests");
        Instant endedAt = Instant.now();
        long duration = Duration.between(startedAt, endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des test: {0} ms", duration));

    }

    //appel avant chaque test
    @BeforeEach
    public void initOffer() {
        offer = new Offer();
        System.out.println("Appel avant chaque test");
    }

    //appel après chaque test
    @AfterEach
    public void undefOffer() {
        System.out.println("Appel après chaque test");
        offer = null;
    }


   @Test
    public  void OfferTest(){

       System.out.println("test");
       final long id = 1;
       final String company = "wesharejob";
       final String post = "premier post";
       final String city = "paris";
       final String url = "www.wesharejob.com";
       final Boolean is_active = false;
       final Date dateBatch = new Date();
       final String  date_scrap_offer = "23/04/2021";
       final String username = "user";

        Offer offer = new Offer();

        offer.setId(id);
        offer.setCompany(company);
        offer.setPost(post);
        offer.setCity(city);
        offer.setUrl(url);
        offer.setIs_active(is_active);
        offer.setDateBatch(dateBatch);
        offer.setDate_scrap_offer(date_scrap_offer);
        offer.setUsername(username);

        offer.getId();
        offer.getCompany();
        offer.getCity();
        offer.getUrl();
        offer.getIs_active();
        offer.getDateBatch();
        offer.getDate_scrap_offer();
        offer.getUsername();

    }



}
