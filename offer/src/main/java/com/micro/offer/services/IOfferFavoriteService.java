package com.micro.offer.services;

import com.micro.offer.model.OfferFavorite;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IOfferFavoriteService {


    public List<OfferFavorite>  getOfferFavoriteByAll();

    public List<OfferFavorite> getOfferFavoriteByUsername(String offre);


    List<OfferFavorite> OfferFavoritebyDate(String date);


    Optional<OfferFavorite> getOfferFavoriteById(long id);

    Optional<OfferFavorite> deleteOfferFavoriteById(long id);


    OfferFavorite saveOfferFavorite(OfferFavorite offerFavorite);


    OfferFavorite updateOfferFavorite(OfferFavorite offerFavorite);

    void deleteOfferFavorite(long id);
}
