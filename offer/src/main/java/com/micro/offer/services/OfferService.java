package com.micro.offer.services;

import com.micro.offer.model.Offer;
import com.micro.offer.repositories.OfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OfferService implements IOfferService {


    @Autowired
    OfferRepository offerRepository;


    @Override
    public List<Offer> getAllOffer() {
        return offerRepository.findAll();
    }

    @Override
    public List<Offer> getOfferByUsername(String offer) {
        return offerRepository.findByUsername(offer);
    }

    @Override
    public List<Offer> getOfferByCompanyOrPostOrCity(String company,String city,String post) {
        return offerRepository.findByCompanyOrCityOrPost(company,city,post);
    }

    @Override
    public List<Offer> OfferbyDate(String date) {
        return offerRepository.findALLByAndDateScrapOffer(date);
    }

    @Override
    public Optional<Offer> getOfferById(long id) {
        return offerRepository.findById(id);
    }

    @Override
    public Offer saveOffer(Offer offer) {

        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

        Date date = new Date();

        offer.setDate_scrap_offer(formater.format(date));

        return offerRepository.save(offer);
    }

    @Override
    public Offer updateOffer(Offer offer) {

        offer = offerRepository.save(offer);
        return offer;
    }

    @Override
    public void deleteOffer(long id) {
        Optional<Offer> offer = offerRepository.findById(id);
        if (offer.isPresent()) {
            offerRepository.delete(offer.get());
        }
    }

    @Override
    public Optional<Offer> deleteOfferById(long id) {
        Optional<Offer> offer = offerRepository.findById(id);
        if (offer.isPresent()) {
            offerRepository.delete(offer.get());
            return offer;
        }
        return Optional.empty();
    }
}
