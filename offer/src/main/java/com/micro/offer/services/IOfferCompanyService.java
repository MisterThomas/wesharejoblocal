package com.micro.offer.services;

import com.micro.offer.model.OfferCompany;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IOfferCompanyService {

    public List<OfferCompany> getAllOfferCompany();

    public List<OfferCompany> getOfferByUsername(String offerCompany);

    public List<OfferCompany> getOfferByCompanyOrPostOrCity(String compagny,String post, String city);

    List<OfferCompany> OfferCompanybyDate(Date date);

    Optional<OfferCompany> getOfferCompanyById(long id);

    Optional<OfferCompany> deleteOfferCompanyById(long id);


    OfferCompany saveOfferCompany(OfferCompany offerCompany);


    OfferCompany updateOfferCompany(OfferCompany offerCompany);


    void deleteOfferCompany(long id);
}
