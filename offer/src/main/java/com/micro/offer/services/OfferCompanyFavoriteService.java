package com.micro.offer.services;


import com.micro.offer.model.OfferCompanyFavorite;
import com.micro.offer.repositories.OfferCompanyFavoriteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OfferCompanyFavoriteService implements IOfferCompanyFavoriteService {


    @Autowired
    OfferCompanyFavoriteRepository offerCompanyFavoriteRepository;


    @Override
    public List<OfferCompanyFavorite> getOfferCompanyFavoriteAll() {
        return offerCompanyFavoriteRepository.findAll();
    }

    @Override
    public List<OfferCompanyFavorite> getOfferCompanyFavoriteByUsername(String offreCompanyFavorite) {
        return  offerCompanyFavoriteRepository.findByUsername(offreCompanyFavorite);
    }

    @Override
    public List<OfferCompanyFavorite> OfferCompanyFavoritebyDate(Date date) {
        return offerCompanyFavoriteRepository.findALLByAndDateScrapOffer(date);
    }

    @Override
    public Optional<OfferCompanyFavorite> getOfferCompanyFavoriteById(long id) {
        return offerCompanyFavoriteRepository.findById(id);
    }


    @Override
    public OfferCompanyFavorite saveOfferCompanyFavorite(OfferCompanyFavorite offerCompanyFavorite) {
        return offerCompanyFavoriteRepository.save(offerCompanyFavorite);
    }

    @Override
    public OfferCompanyFavorite updateOfferCompanyFavorite(OfferCompanyFavorite offerFavorite) {
        offerFavorite = offerCompanyFavoriteRepository.save(offerFavorite);
        return offerFavorite;
    }

    @Override
    public void deleteOfferCompanyFavorite(long id) {
        Optional<OfferCompanyFavorite> offerCompanyFavorite = offerCompanyFavoriteRepository.findById(id);
        if (offerCompanyFavorite.isPresent()) {
            offerCompanyFavoriteRepository.delete(offerCompanyFavorite.get());
        }
    }

    @Override
    public Optional<OfferCompanyFavorite> deleteOfferCompanyFavoriteById(long id) {
        Optional<OfferCompanyFavorite> offerCompanyFavorite = offerCompanyFavoriteRepository.findById(id);
        if (offerCompanyFavorite.isPresent()) {
            offerCompanyFavoriteRepository.delete(offerCompanyFavorite.get());
            return offerCompanyFavorite;
        }
        return Optional.empty();
    }
}
