package com.micro.offer.services;

import com.micro.offer.model.OfferCompanyFavorite;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IOfferCompanyFavoriteService {

    public List<OfferCompanyFavorite> getOfferCompanyFavoriteAll();

    public List<OfferCompanyFavorite> getOfferCompanyFavoriteByUsername(String offreCompanyFavorite);

    List<OfferCompanyFavorite> OfferCompanyFavoritebyDate(Date date);

    Optional<OfferCompanyFavorite> getOfferCompanyFavoriteById(long id);

    Optional<OfferCompanyFavorite> deleteOfferCompanyFavoriteById(long id);

    OfferCompanyFavorite saveOfferCompanyFavorite(OfferCompanyFavorite offerCompanyFavorite);

    OfferCompanyFavorite updateOfferCompanyFavorite(OfferCompanyFavorite offerCompanyFavorite);

    void deleteOfferCompanyFavorite(long id);
}
