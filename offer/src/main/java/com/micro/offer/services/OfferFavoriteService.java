package com.micro.offer.services;


import com.micro.offer.model.OfferFavorite;
import com.micro.offer.repositories.OfferFavoriteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OfferFavoriteService implements IOfferFavoriteService {


    @Autowired
    OfferFavoriteRepository offerFavoriteRepository;

    @Override
    public List<OfferFavorite> getOfferFavoriteByAll() {
        return offerFavoriteRepository.findAll();
    }

    @Override
    public List<OfferFavorite> getOfferFavoriteByUsername(String offreFavorite) {
        return  offerFavoriteRepository.findByUsername(offreFavorite);
    }

    @Override
    public List<OfferFavorite> OfferFavoritebyDate(String date) {
        return offerFavoriteRepository.findALLByAndDateScrapOffer(date);
    }

    @Override
    public Optional<OfferFavorite> getOfferFavoriteById(long id) {
        return offerFavoriteRepository.findById(id);
    }


    @Override
    public OfferFavorite saveOfferFavorite(OfferFavorite offerFavorite) {
        return offerFavoriteRepository.save(offerFavorite);
    }

    @Override
    public OfferFavorite updateOfferFavorite(OfferFavorite offerFavorite) {
        offerFavorite = offerFavoriteRepository.save(offerFavorite);
        return offerFavorite;
    }

    @Override
    public void deleteOfferFavorite(long id) {
        Optional<OfferFavorite> offerFavorite = offerFavoriteRepository.findById(id);
        if (offerFavorite.isPresent()) {
            offerFavoriteRepository.delete(offerFavorite.get());
        }
    }

    @Override
    public Optional<OfferFavorite> deleteOfferFavoriteById(long id) {
        Optional<OfferFavorite> offerFavorite = offerFavoriteRepository.findById(id);
        if (offerFavorite.isPresent()) {
            offerFavoriteRepository.delete(offerFavorite.get());
            return offerFavorite;

        }
        return Optional.empty();
    }
}
