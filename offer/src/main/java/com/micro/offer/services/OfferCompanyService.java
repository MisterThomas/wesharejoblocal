package com.micro.offer.services;

import com.micro.offer.model.OfferCompany;
import com.micro.offer.repositories.OfferCompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OfferCompanyService implements  IOfferCompanyService {

    @Autowired
    OfferCompanyRepository offerCompanyRepository;


    @Override
    public List<OfferCompany> getAllOfferCompany() {
        return offerCompanyRepository.findAll();
    }

    @Override
    public List<OfferCompany> getOfferByUsername(String offerCompany) {
        return offerCompanyRepository.findByUsername(offerCompany);
    }

    @Override
    public List<OfferCompany> getOfferByCompanyOrPostOrCity(String company, String post, String city) {
        return offerCompanyRepository.findByCompanyOrCityOrPost(company, post, city);
    }

    @Override
    public List<OfferCompany> OfferCompanybyDate(Date date) {
        return offerCompanyRepository.findALLByAndDateScrapOffer(date);
    }

    @Override
    public Optional<OfferCompany> getOfferCompanyById(long id) {
        return offerCompanyRepository.findById(id);
    }


    @Override
    public OfferCompany saveOfferCompany(OfferCompany offerCompany) {

        offerCompany = offerCompanyRepository.save(offerCompany);

        return offerCompany;
    }

    @Override
    public OfferCompany updateOfferCompany(OfferCompany offerCompany) {

        offerCompany = offerCompanyRepository.save(offerCompany);
        return offerCompany;
    }

    @Override
    public void deleteOfferCompany(long id) {
        Optional<OfferCompany> offerCompany = offerCompanyRepository.findById(id);
        if (offerCompany.isPresent()) {
            offerCompanyRepository.delete(offerCompany.get());
        }
    }
    
    @Override
    public Optional<OfferCompany> deleteOfferCompanyById(long id) {

        Optional<OfferCompany> offerCompany = offerCompanyRepository.findById(id);
        if (offerCompany.isPresent()) {
            offerCompanyRepository.delete(offerCompany.get());
            return offerCompany;
        }
        return offerCompany;
    }


}
