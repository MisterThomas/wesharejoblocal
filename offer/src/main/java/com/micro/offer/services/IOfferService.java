package com.micro.offer.services;

import com.micro.offer.model.Offer;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IOfferService {


    public List<Offer> getAllOffer();

    public List<Offer> getOfferByUsername(String offer);


     List<Offer> getOfferByCompanyOrPostOrCity(String company,String city,String post);

    List<Offer> OfferbyDate(String date);

    Optional<Offer> getOfferById(long id);


 //   Optional<Offer> getOfferById(long id);


    Optional<Offer> deleteOfferById(long id);


    Offer saveOffer(Offer offer);


    Offer updateOffer(Offer offer);


    void deleteOffer(long id);



}
