package com.micro.offer.controllers;

import com.micro.offer.model.OfferCompany;
import com.micro.offer.repositories.OfferCompanyRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.micro.offer.services.OfferCompanyService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "/api/offerCompany")
@CrossOrigin(origins = "http://localhost:3001")
public class OfferCompanyController {


    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    OfferCompanyService offerCompanyService;

    @Autowired
    OfferCompanyRepository offerCompanyRepository;



  //  @CrossOrigin
    @GetMapping(path = "/listOfferCompanies")
    public List<OfferCompany> listeOfferCompanys(){
        log.info("list offer");
        List<OfferCompany> offerCompany = offerCompanyService.getAllOfferCompany();
        return offerCompany;
    }


  //  @CrossOrigin
    @GetMapping(path="/listOffersCompanies/search", consumes = "application/json", produces = "application/json")
 //   @ResponseBody
    public List<OfferCompany> listCompanyOrPostOrCity(@RequestParam(value = "company", required = false) String company, @RequestParam(value = "post", required = false) String post, @RequestParam(value = "city", required = false) String city){
        log.info("list listOffers by compagny, post, city");
        List<OfferCompany> offersCompanies = offerCompanyService.getOfferByCompanyOrPostOrCity(company, post, city);
        return offersCompanies;
    }



//    @CrossOrigin
    @PostMapping(path = "/listOffersCompanies/post", consumes = "application/json", produces = "application/json")
 //   @ResponseStatus(HttpStatus.CREATED)
    public OfferCompany  createOfferCompany(@RequestBody @Valid final OfferCompany offerCompany) {
        return offerCompanyService.saveOfferCompany(offerCompany);
    }



  //  @CrossOrigin
    @PutMapping(path="/listOffersCompanies/update/{id}", produces = "application/json")
    public ResponseEntity<OfferCompany>  updateOfferCompany(@PathVariable final Long id, @RequestBody OfferCompany offerDetails) {

        OfferCompany offerCompany = offerCompanyRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Offer not exist with id :" + id));

       // Record found
        offerCompany.setCompany(offerDetails.getCompany());
        offerCompany.setCity(offerDetails.getCity());
        offerCompany.setDescription(offerDetails.getDescription());
        offerCompany.setPost(offerDetails.getPost());

       OfferCompany  offerCompanyUpdated =  offerCompanyService.updateOfferCompany(offerCompany);
            // return value not used !
            return  ResponseEntity.ok(offerCompanyUpdated);
    }



 //   @CrossOrigin
    @DeleteMapping(path="/listOffersCompanies/delete/{id}", produces = "application/json")
 //   @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOfferCompany(@PathVariable final Long id) {
        offerCompanyService.deleteOfferCompany(id);
    }


/*  //  @CrossOrigin
    @DeleteMapping(path="/listOffersCompanies/delete2/{id}", produces = "application/json")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOfferCompany2(@PathVariable final Long id) {
        offerCompanyService.deleteOfferCompanyById(id);
    }*/
}
