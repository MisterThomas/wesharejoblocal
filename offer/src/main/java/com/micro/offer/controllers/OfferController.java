package com.micro.offer.controllers;

import com.micro.offer.model.Offer;
import com.micro.offer.repositories.OfferRepository;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.micro.offer.services.OfferService;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "/api/offers")
@CrossOrigin(origins = "http://localhost:3000")
public class OfferController {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OfferService offerService;

    @Autowired
    private OfferRepository offerRepository;


    @CrossOrigin
    @GetMapping(path="/listOffers", produces = "application/json")
    public List<Offer> getAllOffers(){
        log.info("list offers");
        List<Offer> offer = offerService.getAllOffer();
        return offer;
    }



    @CrossOrigin
    @GetMapping("/listOffers/search")
    public List<Offer> listCompanyOrPostOrCity(@RequestParam(value = "company", required = false) String company, @RequestParam(value = "city", required = false) String city,@RequestParam(value = "post", required = false) String post){
        log.info("list listOffers by company, city, post");
        List<Offer> offers = offerService.getOfferByCompanyOrPostOrCity(company,city,post);
        return offers;
    }


/*    @PostMapping("/api/Livre/listeLivres/recherche")
    @ResponseBody
    public List<Livre> listeLivresAuteur(@RequestParam(value = "auteur", required = false) String auteur, @RequestParam(value = "titre", required = false) String titre){
        log.info("liste livres auteur by titre");
        List<Livre> livres = livreRepository.findByAuteurOrTitre(auteur, titre);
        return livres;
    }*/


    @CrossOrigin
    @GetMapping(path="/listOffers/id/{id}", produces = "application/json")
    public Optional<Offer> getOffer(@PathVariable final Long id) {
        log.info("liste listOffers by id");
        return offerService.getOfferById(id);
    }


    @CrossOrigin
    @GetMapping(path="/listOffers/username/{username}", produces = "application/json")
    public List<Offer> getOffer(@PathVariable final String username) {
        log.info("liste listOffers by username");

        List<Offer> offerList = offerService.getOfferByUsername(username);
        return  offerList;
    }

    @CrossOrigin
    @PostMapping(path="/listOffers/post", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Offer saveOffer(@RequestBody @Valid final Offer offer) {

        offer.setId(2L);

        Date date = new Date();

        offer.setDateBatch(date);

        return offerService.saveOffer(offer);
    }


    @CrossOrigin
    @PutMapping(path="/listOffers/update/{id}", produces = "application/json")
    public ResponseEntity <Offer> updateOffer(@PathVariable Long id, @RequestBody Offer offerDetails) {
        Offer offer = offerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Offer not exist with id :" + id));

        offer.setCompany((offerDetails.getCompany()));
        offer.setPost((offerDetails.getPost()));
        offer.setCity(offerDetails.getCity());
        offer.setUrl(offerDetails.getUrl());

        Offer updatedOffer = offerService.saveOffer(offer);
        return ResponseEntity.ok(updatedOffer);
    }


    @DeleteMapping(path="/listOffers/delete/{id}", produces = "application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOffer(@PathVariable final Long id) {
        offerService.deleteOffer(id);
    }






}
