package com.micro.offer.controllers;

import com.micro.offer.model.OfferCompany;
import com.micro.offer.model.OfferCompanyFavorite;
import com.micro.offer.model.OfferFavorite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.micro.offer.services.OfferCompanyFavoriteService;
import com.micro.offer.services.OfferCompanyService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "/api/offerCompanyFavorites")
@CrossOrigin(origins = "http://localhost:3001")
public class OfferCompanyFavoriteController {


    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OfferCompanyFavoriteService offerCompanyFavoriteService;

    @Autowired
    private OfferCompanyService offerCompanyService;



    @GetMapping("/listOffersCompanyFavorites")
    public List<OfferCompanyFavorite> getAllOffers(){
        log.info("list offers");
        List<OfferCompanyFavorite> offerCompanyFavorites = offerCompanyFavoriteService.getOfferCompanyFavoriteAll();
        return offerCompanyFavorites;
    }

    @GetMapping("/listOffersCompanyFavorites/{id}")
    public Optional<OfferCompanyFavorite> getOfferCompanyFavorite(@PathVariable final Long id) {
        log.info("list listOffers by id");
        return offerCompanyFavoriteService.getOfferCompanyFavoriteById(id);
    }


    @DeleteMapping(path="/listOffersCompanyFavorite/delete/{id}",  produces = "application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOfferCompanyFavorite(@PathVariable final Long id) {
        offerCompanyFavoriteService.deleteOfferCompanyFavorite(id);
    }


    @DeleteMapping("/listOffersCompanyFavorite/delete2/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOfferCompanyFavorite2(@PathVariable final Long id) {
        offerCompanyFavoriteService.deleteOfferCompanyFavoriteById(id);
    }


    @PostMapping(path="/listOffersCompany/{id}/add-OfferCompanyFavorite", consumes = "application/json", produces = "application/json")
    public OfferCompanyFavorite getAddOfferCompanyFavorite(@PathVariable("id")  Long id, @RequestBody @Valid OfferCompanyFavorite offerCompanyFavorite) {
        log.info("list add listOffers favorite");
        OfferCompany offerCompany = offerCompanyService.getOfferCompanyById(id).get();
        offerCompanyFavorite.setOffercompany(offerCompany);
        return offerCompanyFavoriteService.saveOfferCompanyFavorite(offerCompanyFavorite);
    }

    @GetMapping(path="/listOffersCompanyFavorite/username/{username}", produces = "application/json")
    public List<OfferCompanyFavorite> getOfferCompanyFavoriteUsername(@PathVariable final String username) {
        log.info("liste listOffersCompanyFavorite by username");

        List<OfferCompanyFavorite> offerCompanyFavoriteList = offerCompanyFavoriteService.getOfferCompanyFavoriteByUsername(username);
        return offerCompanyFavoriteService.getOfferCompanyFavoriteByUsername(username);
    }



    @PostMapping(path="/listOffersCompanyResp/{id}/add-OfferCompanyFavorite", consumes = "application/json", produces = "application/json")
    public ResponseEntity<OfferCompanyFavorite> getOfferFavoriteResp(@PathVariable("id")  Long id, @RequestBody @Valid OfferCompanyFavorite offerCompanyFavorite) {
        log.info("list add listOffers favorite");
        OfferCompany offerCompany = offerCompanyService.getOfferCompanyById(id).get();
        offerCompanyFavorite.setOffercompany(offerCompany);
        offerCompanyFavorite =  offerCompanyFavoriteService.saveOfferCompanyFavorite(offerCompanyFavorite);
        return new ResponseEntity<OfferCompanyFavorite>(offerCompanyFavorite, HttpStatus.OK);
    }
}
