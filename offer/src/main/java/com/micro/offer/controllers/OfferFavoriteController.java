package com.micro.offer.controllers;

import com.micro.offer.model.Offer;
import com.micro.offer.model.OfferFavorite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.micro.offer.services.OfferFavoriteService;
import com.micro.offer.services.OfferService;

import javax.validation.Valid;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "/api/offerFavorites")
@CrossOrigin(origins = "http://localhost:3000")
public class OfferFavoriteController {


    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private OfferFavoriteService offerFavoriteService;


    @Autowired
    private OfferService offerService;

    @CrossOrigin
    @GetMapping("/listOffersFavorites")
    public List<OfferFavorite> getAllOffers(){
        log.info("list offers");
        List<OfferFavorite> offerFavorites = offerFavoriteService.getOfferFavoriteByAll();
        return offerFavorites;
    }

    @CrossOrigin
    @GetMapping(path="/listOffersFavorites/username/{username}", produces = "application/json")
    public List<OfferFavorite> getOfferFavoriteUsername(@PathVariable final String username) {
        log.info("liste listOffers by username");

        List<OfferFavorite> offerFavoriteList = offerFavoriteService.getOfferFavoriteByUsername(username);
        return offerFavoriteList;
    }

    @CrossOrigin
    @DeleteMapping("/listOffersFavorites/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOfferFavorite(@PathVariable final Long id) {
        offerFavoriteService.deleteOfferFavorite(id);
    }


    @CrossOrigin
    @GetMapping("/listOffersFavorites/{id}")
    public Optional<OfferFavorite> getOfferFavorite(@PathVariable final Long id) {
        log.info("list listOffers by id");
        return offerFavoriteService.getOfferFavoriteById(id);
    }


    @CrossOrigin
    @PostMapping(path= "/listOffers/{id}/add-offerFavorite", produces = "application/json")
    public ResponseEntity<OfferFavorite> offerFavoritePost(@PathVariable("id")  Long id, @RequestBody @Valid OfferFavorite offerFavorite) {
        log.info("list add listOffers favorite");

        Offer offer = offerService.getOfferById(id).get();
        offerFavorite.setOffer(offer);

        offerFavorite.setId(2L);

        Date date = new Date();

        offerFavorite.setDateBatch(date);

        List<OfferFavorite> offerFavoriteList = offerFavoriteService.getOfferFavoriteByUsername(offerFavorite.getUsername());

        Iterator<OfferFavorite> iterOfferFavorite = offerFavoriteList.iterator();

        while (iterOfferFavorite.hasNext()) {

            OfferFavorite offerFavoriteBean = iterOfferFavorite.next();

            if (offer.getId() == offerFavoriteBean.getOffer().getId()) {
                return null;
            }
        }
            OfferFavorite offerFavoriteEntity =  offerFavoriteService.saveOfferFavorite(offerFavorite);

       return ResponseEntity.ok(offerFavoriteEntity);
    }




}
