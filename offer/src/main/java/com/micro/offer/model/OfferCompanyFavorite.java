package com.micro.offer.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="offerCompanyFavorite")
public class OfferCompanyFavorite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "date_scrap_offer")
    private Date date_scrap_offer;

    @Column(nullable = false, name = "is_active")
    private Boolean is_active;

    @ManyToOne
    @JoinColumn(name = "offercompany_id", nullable = true)
    private OfferCompany offercompany;


    @JsonCreator
    public OfferCompanyFavorite(String username, Date date_scrap_offer, Boolean is_active) {
        this.username = username;
        this.date_scrap_offer = date_scrap_offer;
        this.is_active = is_active;

    }


    public OfferCompanyFavorite(Long id,String username, Date date_scrap_offer, Boolean is_active) {
        this.id = id;
        this.username = username;
        this.date_scrap_offer = date_scrap_offer;
        this.is_active = is_active;

    }


    public OfferCompanyFavorite(Long id,String username, Date date_scrap_offer, Boolean is_active,OfferCompany offercompany) {
        this.id = id;
        this.username = username;
        this.date_scrap_offer = date_scrap_offer;
        this.is_active = is_active;
        this.offercompany = offercompany;

    }
    public OfferCompanyFavorite() {

    }

    public long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public OfferCompany getOffercompany() {
        return offercompany;
    }

    public void setOffercompany(final OfferCompany offercompany) {
        this.offercompany = offercompany;
    }


    public Date getDate_scrap_offer() {
        return date_scrap_offer;
    }

    public void setDate_scrap_offer(Date date_scrap_offer) {
        this.date_scrap_offer = date_scrap_offer;
    }


    @Override
    public String toString() {
        return "OfferCompanyFavorite [" +
                "id=" + id +
                ", username=" + username +
                ", date_scrap_offer=" + date_scrap_offer +
                ", is_active=" + is_active +
                ", offercompany=" + offercompany +
                "]";
    }
}
