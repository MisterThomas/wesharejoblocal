package com.micro.offer.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="offerFavorite")
public class OfferFavorite {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username")
    private String username;


    @Column(name = "dateBatch")
    private Date dateBatch = new Date();

    @Column(name = "date_scrap_offer")
    private String date_scrap_offer;

    @Column(nullable = false, name = "is_active")
    private Boolean is_active;

    @ManyToOne
    @JoinColumn(name = "offer_id", nullable = true)
    private Offer offer;

    @JsonCreator
    public OfferFavorite(String username,Date dateBatch, String date_scrap_offer, Boolean is_active) {
        this.username = username;
        this.dateBatch = dateBatch;
        this.date_scrap_offer = date_scrap_offer;
        this.is_active = is_active;

    }


    public OfferFavorite(Long id,String username,Date dateBatch, String date_scrap_offer, Boolean is_active) {
        this.id = id;
        this.username = username;
        this.date_scrap_offer = date_scrap_offer;
        this.is_active = is_active;

    }

    public OfferFavorite() {

    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }


    public Offer getOffer() {
        return offer;
    }

    public void setOffer(final Offer offer) {
        this.offer = offer;
    }

    public Date getDateBatch() {
        return dateBatch;
    }

    public void setDateBatch(Date dateBatch) {
        this.dateBatch = dateBatch;
    }

    public String getDate_scrap_offer() {
        return date_scrap_offer;
    }

    public void setDate_scrap_offer(String date_scrap_offer) {
        this.date_scrap_offer = date_scrap_offer;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }


    @Override
    public String toString() {
        return "OfferFavorite [" +
                "id=" + id +
                ", username=" + username +
                ", dateBatch=" + dateBatch +
                ", date_scrap_offer=" + date_scrap_offer +
                ", is_active=" + is_active +
                ", offer=" + offer +
                "]";
    }
}
