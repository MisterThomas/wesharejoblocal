package com.micro.offer.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name="offerCompany")
public class OfferCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "company")
    private String company;

    @Column(name = "post")
    private String post;

    @Column(name = "description")
    private String description;

    @Column(name = "city")
    private String city;


    @Column(name = "date_offer")
    private Date date_offer = new Date();

    @Column(name = "username")
    private String username;

    @Column(name = "url")
    private String url;

    @Column(name = "is_active")
    private Boolean is_active;


    @JsonCreator
    public OfferCompany(@JsonProperty("company") String company,@JsonProperty("post") String post,@JsonProperty("description") String description,@JsonProperty("city") String city,@JsonProperty("date_offer")  Date date_offer,@JsonProperty("username") String username,@JsonProperty("url") String url,@JsonProperty("is_active") Boolean is_active) {
        this.company = company;
        this.post = post;
        this.description = description;
        this.city = city;
        this.date_offer = date_offer;
        this.username = username;
        this.url = url;
        this.is_active = is_active;
    }


    public OfferCompany(Long id, String company, String post, String description, String city, Date date_offer, String username, String url, Boolean is_active) {
        this.id =id;
        this.company = company;
        this.post = post;
        this.description = description;
        this.city = city;
        this.date_offer = date_offer;
        this.username = username;
        this.url = url;
        this.is_active = is_active;
    }


    @JsonIgnore
    @OneToMany(mappedBy = "offercompany", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<OfferCompanyFavorite> offerCompanyFavorite;

    public OfferCompany() {

    }


    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(final String company) {
        this.company = company;
    }

    public String getPost() {
        return post;
    }

    public void setPost(final String post) {
        this.post = post;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public Date getDate_offer() {
        return date_offer;
    }

    public void setDate_offer(Date date_offer) {
        this.date_offer = date_offer;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Set<OfferCompanyFavorite> getOfferCompanyFavorite() {
        return offerCompanyFavorite;
    }

    public void setOfferCompanyFavorite(Set<OfferCompanyFavorite> offerCompanyFavorite) {
        this.offerCompanyFavorite = offerCompanyFavorite;
    }

    @Override
    public String toString() {
        return "OfferCompany [" +
                "id=" + id +
                ", company=" + company +
                ", post=" + post +
                ", description=" + description +
                ", city=" + city  +
                ", date_offer=" + date_offer +
                ", username=" + username +
                ", url=" + url  +
                ", is_active=" + is_active +
                "]";
    }
}
