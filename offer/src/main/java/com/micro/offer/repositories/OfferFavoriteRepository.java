package com.micro.offer.repositories;

import com.micro.offer.model.OfferFavorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OfferFavoriteRepository extends JpaRepository<OfferFavorite, Long> {
    // add custom queries here

    List<OfferFavorite> findByUsername(String offer);


    @Query("SELECT o FROM OfferFavorite  o WHERE o.date_scrap_offer = o.date_scrap_offer")
    List<OfferFavorite> findALLByAndDateScrapOffer(String date);

}
