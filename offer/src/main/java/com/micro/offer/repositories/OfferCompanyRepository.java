package com.micro.offer.repositories;

import com.micro.offer.model.OfferCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OfferCompanyRepository extends JpaRepository<OfferCompany, Long> {
    // add custom queries here

    List<OfferCompany> findByUsername(String offer);


    List<OfferCompany> findByCompanyOrCityOrPost(String company, String city, String post);


    @Query("SELECT o FROM OfferCompany  o WHERE o.date_offer = o.date_offer")
    List<OfferCompany> findALLByAndDateScrapOffer(Date date);
}
