package com.micro.offer.repositories;

import com.micro.offer.model.OfferCompanyFavorite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OfferCompanyFavoriteRepository extends JpaRepository<OfferCompanyFavorite, Long> {
    // add custom queries here

    List<OfferCompanyFavorite> findByUsername(String offer);


    @Query("SELECT o FROM OfferCompanyFavorite  o WHERE o.date_scrap_offer = o.date_scrap_offer")
    List<OfferCompanyFavorite> findALLByAndDateScrapOffer(Date date);

}
