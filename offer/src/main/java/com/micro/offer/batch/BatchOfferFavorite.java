package com.micro.offer.batch;



import com.micro.offer.model.OfferFavorite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.micro.offer.services.OfferFavoriteService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class BatchOfferFavorite {

    @Autowired
    OfferFavoriteService offerFavoriteService;

 //   @Scheduled(cron = "*/10 * * * * *")
    @Scheduled(cron = "0 0/58 18 * * *")
    public void batchOfferCompanyDelete() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateBatch = dateFormat.format(date);

        Calendar calendar = Calendar.getInstance();

        List<OfferFavorite> offerFavoriteList = offerFavoriteService.OfferFavoritebyDate(dateBatch);

        Iterator<OfferFavorite> iterOfferFavorite = offerFavoriteList.iterator();

        while (iterOfferFavorite.hasNext()){

                OfferFavorite offerFavoriteBean = iterOfferFavorite.next();

                Date date1 = offerFavoriteBean.getDateBatch();
                Calendar c = Calendar.getInstance();
                c.setTime(date1);
                c.add(Calendar.DAY_OF_WEEK, 28);
                Date date2 = c.getTime();

                if (calendar.getTime().compareTo(date2) >= 0) {
                    offerFavoriteService.deleteOfferFavoriteById(offerFavoriteBean.getId());
                    System.out.println("offreFavorite supprimer");
                }else {
                    System.out.println("offreFavorite pas supprimer");
                }
            }
    }
}
