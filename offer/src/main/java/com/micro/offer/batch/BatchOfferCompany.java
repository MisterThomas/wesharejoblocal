package com.micro.offer.batch;

import com.micro.offer.model.OfferCompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.micro.offer.services.OfferCompanyService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class BatchOfferCompany {


    @Autowired
    OfferCompanyService offerCompanyService;

    @Scheduled(cron = "0 0/05 19 * * *")
    public void batchOfferCompanyDelete() {
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
        Date date = new Date();

        List<OfferCompany> offerCompanyList = offerCompanyService.OfferCompanybyDate(date);


        Iterator<OfferCompany> iterOfferCompany = offerCompanyList.iterator();


        while (iterOfferCompany.hasNext()) {

            OfferCompany offerCompanyBean = iterOfferCompany.next();

            Date date1 = offerCompanyBean.getDate_offer();
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            c.add(Calendar.DAY_OF_WEEK, 28);
            Date date2 = c.getTime();

            if (calendar.getTime().compareTo(date2) >= 0){
                offerCompanyService.deleteOfferCompanyById(offerCompanyBean.getId());
            }
        }

    }
}
