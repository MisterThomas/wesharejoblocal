package com.micro.offer.batch;


import com.micro.offer.model.OfferCompanyFavorite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.micro.offer.services.OfferCompanyFavoriteService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@Component
public class BatchOfferCompanyFavorite {


    @Autowired
    OfferCompanyFavoriteService offerCompanyFavoriteService;

    @Scheduled(cron = "0 0/03 19 * * *")
    public void batchOfferFavoriteDelete() {
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
        Date date = new Date();

        List<OfferCompanyFavorite> offerCompanyFavoriteList = offerCompanyFavoriteService.OfferCompanyFavoritebyDate(date);

        Iterator<OfferCompanyFavorite> iterOfferCompanyFavorite = offerCompanyFavoriteList.iterator();


        while (iterOfferCompanyFavorite.hasNext()) {

            OfferCompanyFavorite offerCompanyBean = iterOfferCompanyFavorite.next();

            Date date1 = offerCompanyBean.getDate_scrap_offer();
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            c.add(Calendar.WEEK_OF_MONTH, 4);

            if (calendar.getTime().compareTo(date1) >= 0) {
                offerCompanyFavoriteService.deleteOfferCompanyFavoriteById(offerCompanyBean.getId());
                System.out.println("delete offer");
            }
        }
    }
}
