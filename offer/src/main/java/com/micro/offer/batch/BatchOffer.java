package com.micro.offer.batch;


import com.micro.offer.model.Offer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.micro.offer.services.OfferService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class BatchOffer {

    @Autowired
    OfferService offerService;

 //   @Scheduled(cron = "*/10 * * * * *")
    @Scheduled(cron = "0 0/01 19 * * *")
  //    @Scheduled(cron = "*/10 * * * * *")
    public void batchOfferDelete() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateBatch = dateFormat.format(date);

        List<Offer> offerList = offerService.OfferbyDate(dateBatch);


        Calendar calendar = Calendar.getInstance();
        Date dateCalendar = calendar.getTime();

        Iterator<Offer> iterOffer = offerList.iterator();

        while (iterOffer.hasNext()) {

            Offer offerBean = iterOffer.next();

            Date date1 = offerBean.getDateBatch();
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            c.add(Calendar.DAY_OF_WEEK, 28);
            Date date2 = c.getTime();



            if (calendar.getTime().compareTo(date2) > 0){
                offerService.deleteOffer(offerBean.getId());
                System.out.println("offre supprimer");
            }
            else {
                System.out.println("offre pas supprimer");
            }
        }
    }
}
